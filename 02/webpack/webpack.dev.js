"use strict";

const merge = require("webpack-merge");
const common = require("./webpack.common.js");
const path = require("path");

const basePath = __dirname;
const distPath = "src";

module.exports = merge(common, {
  mode: "development",
	devtool: "source-map",
	devServer: 
	{
		contentBase: path.resolve(basePath, distPath),
    port: 3500,
		open: true,
		compress: true
  }
});