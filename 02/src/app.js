import "./styles/styles.scss";
import "./static/logo.png";

import { showAlert } from "./scripts/messages";
import $ from "jquery";
import { v4 } from "uuid";

// Contenido de JavaScript. 
document.getElementById("btn-alert").addEventListener("click", showAlert);
$("#btn-alert-jquery").click(() => window.alert("Identificador único UUID: " + v4()) );