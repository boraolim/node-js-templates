<h1>Plantilla base de una aplicación en WebPack</h1>
<p>Ejemplo base de un esqueleto de una plantilla Frontend con WebPack y sus dependencias.</p>
<p>Realizar la instralación de los siguientes componentes, en el órden siguiente:</p>
<ol>
  <li>Crear el archivo de configuración de Node.js: npm init -y</li>
  <li>Instalar webpack, webpack-cli y webpack-dev-server: npm install --save-dev webpack webpack-cli webpack-dev-server</li>
  <li>Instalar HTML para webpack: npm install --save-dev html-webpack-plugin</li>
  <li>Instalar CSS3 plugin para webpack: npm install --save-dev style-loader css-loader node-sass postcss-loader sass-loader</li>
  <li>Instalar componentes de carga de archivos y rutas para webpack: npm install --save-dev file-loader url-loader</li>
  <li>Instalar limpiador de archivos de pre-producción para webpack: npm install --save-dev clean-webpack-plugin</li>
  <li>Instalar extractor de CSS3 en archivos de producción: npm install --save-dev mini-css-extract-plugin</li>
  <li>Instalar iconos de la aplicación web: npm install --save-dev favicons-webpack-plugin</li>
  <li>Instalar webpack merge para unir archivos de configuración: npm install --save-dev webpack-merge</li>
</ol>
<h3>Fuentes de consulta:</h3>
<ul>
  <li><a href="https://www.youtube.com/watch?v=vF2emKbaP4M">Curso b&aacute;sico de Webpack 4</a></li>
  <li><a href="https://areknawo.com/how-to-setup-webpack-config/">How to setup Webpack config.</a></li>
  <li><a href="https://www.sitepoint.com/bundle-static-site-webpack/">How to Bundle a Simple Static Site Using Webpack.</a></li>
  <li><a href="https://github.com/johnagan/clean-webpack-plugin">Clean-webpack-plugin.</a></li>
  <li><a href="https://github.com/webpack/webpack-dev-server">webpack-dev-server.</a></li>
  <li><a href="https://github.com/jantimon/html-webpack-plugin">HTML Webpack Plugin.</a></li>
  <li><a href="https://github.com/webpack-contrib/style-loader">Style Loader.</a></li>
  <li><a href="https://www.npmjs.com/package/favicons-webpack-plugin">Favicons Webpack Plugin.</a></li>
  <li><a href="https://gist.github.com/tejasbubane/9ae4d2e099bef97b3b118f99a163294e">Creación de entornos para WebPack.</a></li>
  <li><a href="https://medium.com/@afdiaz_/webpack-paso-a-paso-2fdf095d5bdd">Webpack paso a paso.</a></li>
</ul>
<hr>
