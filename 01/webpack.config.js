// Archivo de configuración de webpack para desarrollo y producción.
// Autor: Olimpo Bonilla Ramírez.
// Fecha: 2020-04-26.
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HmtlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const path = require("path");

module.exports = {
    entry: "./src/app.js",
    output: {
		path: path.join(__dirname, "dist"),
      	filename: "bundle.js"
	},
    devServer: {
		// port: 3500,
		compress: true,
		contentBase: path.join(__dirname, "src")
	},
	module: {
		rules: [
				{ test: /\.scss$/i, 
					use: [
						// { loader: "style-loader" }, 
						{ loader: MiniCssExtractPlugin.loader },
						{ loader: "css-loader", options: { importLoaders: 1 } },
						{ loader: "sass-loader"}
					] 
				},
				{
					test: /\.(svg|gif|png|eot|woff|ttf)$/i,
					use: [ 
						{ loader: "file-loader" } 
					]
				},
				{
					test: /\.(svg|gif|png|eot|woff|ttf)$/i,
					use: [
						{ loader: "url-loader" }
					]
				}
			],
		},		
    plugins: [
			new CleanWebpackPlugin(),
			new MiniCssExtractPlugin({
        	filename: "bundle.css"
		}),
      	new HmtlWebPackPlugin({
			title: "Mi primera aplicación de WebPack",
        	template: "./src/index.html"
		}),
		new BrowserSyncPlugin({
			host: "localhost",
			port: 3500,
			proxy: "http://localhost:8080/"
		},
		{
			reload: false
		})
    ]
}