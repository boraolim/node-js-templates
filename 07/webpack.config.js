/* eslint-disable @typescript-eslint/no-var-requires */
/// <binding BeforeBuild='Run - Development' />
"use strict";

const path = require("path");
const webpack = require("webpack");
const cssnano = require("cssnano");
const postcss = require("postcss");
const autoprefixer = require("autoprefixer");
const TerserPlugin = require("terser-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const publicPathProd = "/";

/* Punto de entrada para la compilación de WebPack. */
const entryPath = [ "./main.ts" ];

/* Carpeta raíz de la aplicación web. */
const basePath = __dirname;

/* Carpeta donde se encuentran los recursos. */
const appPath = "/src";

/* Carpeta destino donde se compilaran los archivos js y css finales. */
const distPath = "/dist";

/* Carpeta destino donde se compilaran las fuentes de la aplicación web. */
const fontPath = "/fonts";

/* Carpeta destino donde se compilaran los archivos de imagenes. */
const imgPath = "/static";

/* Nombre del archivo final de JavaScript. */
const distjs = "js/app.min.js";

/* Nombre del archivo final de estilos CSS3. */
const distcss = "css/main.min.css";

/* Definición de compilación. */
module.exports = {
  mode: "production",
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".css", ".scss", ".sass", ".css", "jpeg", "jpg", "png", "gif", "webp"]
  },
  context: path.join(__dirname, appPath),
  entry: entryPath,
  output: {
    /* Esta sección es para la generación de los archivos de JavaScript finales. */
    path: path.join(basePath, distPath),
    filename: distjs,
  },
  performance: {
    hints: false,
    maxEntrypointSize: 2000000,
    maxAssetSize: 2000000
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        exclude: /(node_modules|bower_components)/,
        use: [{
          loader: "raw-loader"
        }]
      },
      {
        test: /(\.ts|.tsx)$/i,
        exclude: /(node_modules|bower_components)/,
        use: ["ts-loader"]
      },
      {
        test: /\.(sa|sc|c)ss$/i,
        exclude: /(node_modules|bower_components)/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: "css-loader" },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                autoprefixer: {
                  browser: ["last 2 versions"]
                },
                plugins: () => [autoprefixer, postcss]
              }
            }
          },
          { loader: "sass-loader" }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|webp)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: imgPath,
              publicPath: "../static",
              mimeType: "application/octet-stream",
              useRelativePath: true
            }
          },
          {
            loader: "image-webpack-loader",
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              // optipng.enabled: false will disable optipng
              optipng: {
                enabled: false
              },
              pngquant: {
                quality: [0.65, 0.90],
                speed: 4
              },
              gifsicle: {
                interlaced: false,
              },
              // the webp option will enable WEBP
              webp: {
                quality: 75
              }
            }
          }
        ]
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: fontPath,
          publicPath: "../fonts",
          mimeType: "application/octet-stream",
          useRelativePath: true
        }
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: fontPath,
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      },
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: fontPath,
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      },
      {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: fontPath,
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: fontPath,
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      },
      {
        test: /\.otf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: fontPath,
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      }
    ]
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          compress: true,
          minimize: true,
          extractComments: false,
          sourceMap: false,
          output: {
            comments: false
          }
        }
      })
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      inject: "body",
      template: "./index.html",
      filename: "./index.html",
      favicon: "./favicon.ico",
      title: "ui-rooter",
      base: {
        "href": (process.env.NODE_ENV === "production") ? publicPathProd : "/"
      },
      publicPath: (process.env.NODE_ENV === "production") ? publicPathProd : "/",
      minify: {
        html5: true,
        compress: true,
        caseSensitive: true,
        removeComments: true,
        useShortDoctype: true,
        collapseWhitespace: true,
        removeEmptyElements: false,
        removeAttributeQuotes: false,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new OptimizeCSSAssetsPlugin({
      cssProcessor: cssnano,
      cssProcessorPluginOptions: {
        preset: [ "default", { discardComments: { removeAll: true } }],
      },
      canPrint: true
    }),
    new MiniCssExtractPlugin({
      path: path.join(basePath, distPath),
      filename: distcss,
      compress: true,
      comments: false,
      sourceMap: false,
      discardComments: { removeAll: true }
    })
  ]
};
