/* eslint-disable no-multi-spaces */
/* Platilla esLint para la configuración y validación de JavaScript. */
/* Autor: Olimpo Bonilla Ramírez. */
// "off" or 0 - turn the rule off.
// "warn" or 1 - turn the rule on as a warning (doesn't affect exit code).
// "error" or 2 - turn the rule on as an error (exit code will be 1).

module.exports = {
  /* Entorno de desarrollo:
     https://eslint.org/docs/user-guide/configuring#specifying-environments */
  "env" : {
    "browser" : true,         /* El objetivo del código es ejecutar en navegadores */
    "es6" : true,             /* El código estará escrito en ES6 */
    "commonjs" : true,        /* Javascript común. */
    "jquery" : true           /* Si jQuery es requerido. */
  },
  /* Activar variables globales.
     https://stackoverflow.com/questions/39053562/eslint-no-undef-and-webpack-plugin */
  "globals" : {
    "API_URL" : true,
    "SERVICE_URL" : true,
    "IsProductive" : true
  },
  /* Plugins de eslint para TypeScript. */
  "plugins": [
    "@typescript-eslint"
  ],
  /* Activa las reglas marcadas con ✓ en la documentación oficial de ESLint:
     https://eslint.org/docs/rules/ y el paquete eslint-config-standard */
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "standard"
  ],
  /* Opciones de parseo:
     https://eslint.org/docs/user-guide/configuring#specifying-parser-options */
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": 2018,     /* Establece la versión de ECMAScript que se usará */
    "sourceType": "module"   /* Indica si se usan módulos ES6 o solo scripts */
  },
  /* Reglas de ESLint personalizadas (sobreescriben a anteriores):
     https://eslint.org/docs/rules/ */
  "rules": {
    "@typescript-eslint/no-explicit-any": 0,
    "@typescript-eslint/no-namespace": "off",
    "@typescript-eslint/explicit-module-boundary-types":0,
    "@typescript-eslint/ban-types": 0,
    "@typescript-eslint/prefer-namespace-keyword": "off",
    " @typescript-eslint/no-empty-function": "off",
    "@typescript-eslint/no-empty-function": "off",
    /* No permitir constructor innecesario: https://eslint.org/docs/rules/no-useless-constructor */
    "no-useless-constructor": "error",
    /* Variables no definidas: https://eslint.org/docs/rules/no-undef */
    "no-undef": [ "error", { "typeof": true }],
    /* ´Variables no usadas: https://eslint.org/docs/rules/no-unused-vars#disallow-unused-variables-no-unused-vars */
    "no-unused-vars": [ "error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false } ],
    /* Forzar la eclaración de variables en formato camelcase: https://eslint.org/docs/rules/camelcase */
    "camelcase" : [ "error", { "properties" : "always" }],
    /* Indentación a 2 espacios: https://eslint.org/docs/rules/indent */
    "indent": [ "error", 2, { "SwitchCase": 1, "outerIIFEBody": 2, "VariableDeclarator": 1, "ArrayExpression": 1, "ObjectExpression": 1, "MemberExpression": 1, "FunctionExpression": { "body": 1 }, "ArrayExpression": 1 }],
    /* Finales de línea de UNIX: https://eslint.org/docs/rules/linebreak-style */
    "linebreak-style": [ "error", "unix" ],
    /* Uso de comillas dobles para strings: https://eslint.org/docs/rules/quotes */
    "quotes" : [ "error", "double" ],
    /* Uso de punto y coma obligatorio: https://eslint.org/docs/rules/semi */
    "semi" : [ "error", "always" ],
    /* Evitar la redeclaración de variables: https://eslint.org/docs/rules/no-redeclare */
    "no-redeclare" : [ "error", { "builtinGlobals" : true }],
    /* Permitir espacios en blanco al final de las líneas vacías: https://eslint.org/docs/rules/no-trailing-spaces */
    "no-trailing-spaces" : [ "error", { "skipBlankLines" : true }],
    /* Imponer un espacio dentro de paréntesis: https://eslint.org/docs/rules/space-in-parens */
    "space-in-parens" : [ "error", "never" ],
    /* Rechazar nueva línea al final de los archivos: https://eslint.org/docs/rules/eol-last */
    "eol-last" : [ "error", "always" ],
    /* Requerir o rechazar un espacio antes de la función paréntesis: https://eslint.org/docs/rules/space-before-function-paren */
    "space-before-function-paren" : [ "error", "never" ],
    /* Requerir o rechazar el espacio entre identificadores de función y sus invocaciones (func-call-spacing): https://eslint.org/docs/rules/func-call-spacing */
    "func-call-spacing" : [ "error", "never" ],
    /* No permitir espacios mixtos y pestañas para sangría (sin espacios mixtos y pestañas): https://eslint.org/docs/rules/no-mixed-spaces-and-tabs */
    "no-mixed-spaces-and-tabs" : [ "error", "smart-tabs" ],
    /* No permitir espacios múltiples (sin espacios múltiples): https://eslint.org/docs/rules/no-multi-spaces */
    "no-multi-spaces" : [ "error", { "ignoreEOLComments" : false }],
    /* Requerir comillas alrededor de nombres de propiedades literales de objetos (quote-props): https://eslint.org/docs/rules/quote-props */
    "quote-props" : [ "error", "consistent" ],
    /* No permitir o imponer espacios dentro de los corchetes (espaciado entre paréntesis): https://eslint.org/docs/rules/array-bracket-spacing */
    "array-bracket-spacing" : [ "error", "always" ],
    /* No permitir todas las pestañas: https://eslint.org/docs/rules/no-tabs */
    "no-tabs" : [ "error", { "allowIndentationTabs" : true }],
    /* Requerir o rechazar el relleno dentro de bloques: https://eslint.org/docs/rules/padded-blocks */
    "padded-blocks" : [ "error", "never" ],
    /* Imponer un espacio constante entre claves y valores en las propiedades literales del objeto: https://eslint.org/docs/rules/key-spacing */
    "key-spacing": [ "error", { "beforeColon": false }],
    /* No permitir o aplicar espacios dentro de las propiedades calculadas: https://eslint.org/docs/rules/computed-property-spacing */
    "computed-property-spacing" : [ "error", "never" ],
    /* Requerir notación de punto: https://eslint.org/docs/rules/dot-notation */
    "dot-notation": [ "error", { "allowKeywords": false, "allowPattern": "^[A-Za-z0-9_-]+$" }],
    /* No permitir espacios en blanco antes de propiedades: https://eslint.org/docs/rules/no-whitespace-before-property */
    "no-whitespace-before-property" : [ "error" ],
    /* Imponer un espacio constante antes y después de las palabras clave: https://eslint.org/docs/rules/keyword-spacing */
    "keyword-spacing": ["error", { "before": true }],
    /* Lineas multiples a una: https://eslint.org/docs/rules/no-multiple-empty-lines */
    "no-multiple-empty-lines": [ "error", { "max": 1, "maxEOF": 0 } ],
    /* Habilitar o deshabilitar el uso de "debugger": https://eslint.org/docs/rules/no-debugger */
    "no-debugger" : "error",
    /* Requerir o no permitir comas finales: https://eslint.org/docs/rules/comma-dangle */
    "comma-dangle": [ "error", { "arrays": "never", "objects": "never", "imports": "never", "exports": "never", "functions": "never" } ],
    /* Requerir que los nombres de los constructores comiencen con una letra mayúscula: https://eslint.org/docs/rules/new-cap */
    "new-cap": ["error", { "newIsCap": false }],
    /* Hacer cumplir la colocación de las propiedades del objeto en líneas separadas: https://eslint.org/docs/rules/object-property-newline */
    "object-property-newline": [ "error", { "allowAllPropertiesOnSameLine": true } ]
  }
};
