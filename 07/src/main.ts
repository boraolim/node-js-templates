// Librería de arranque de TypeScript.
// Autor: OLIMPO BONILLA RAMIREZ.

/* Importando estilos CSS3 (general) */
import "./styles/styles.scss";

// AngularJS.
import "angular";
import "angular-aria";
import "angular-animate";
import "angular-block-ui";
import "angular-bootstrap-toggle";
import "angular-bootstrap-toggle-switch";
import "angular-cookies";
import "angular-confirm";
import "angular-ladda";
import "angular-loading-bar";
import "angular-local-storage";
import "angular-material";
import "angular-messages";
import "angular-moment";
import "angular-resource";
import "angular-route";
import "angular-sanitize";
import "angular-sweetalert";
import "angular-translate";
import "angular-translate-storage-local";
import "angular-translate-storage-cookie";
import "angular-translate-loader-partial";
import "angular-translate-interpolation-messageformat";
import "angular-touch";
import "angular-ui-mask";
import "angular-ui-router";
import "angularjs-toaster";
import "angularjs-ui-bootstrap";
import "lodash";
import "ng-dialog";
import "ng-mask";
import "restangular";
import "ui-select";

// jQuery y componentes.
import "jquery";
import "jqueryui";
import "admin-lte";
import "jquery-slimscroll";
import "bootstrap-sweetalert";
import "moment";

// Elementos de AngularJS.
import "./scripts/index";
import "./app.main";
