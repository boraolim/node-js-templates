/* AngularJS */
import * as angular from "angular";
import * as core from "@uirouter/angularjs/lib/stateProvider";
import * as cfg1 from "../src/scripts/bootstrap/app.config";
import * as cfg2 from "../src/scripts/bootstrap/app.run";
import * as cfg3 from "../src/scripts/providers/app.provider.init";

export module ApplicationMain {

  /* Inyector principal. */
  const _injectAll = [
    "ngSanitize",
    "ngAnimate",
    "ngTouch",
    "ngCookies",
    "ngMask",
    "ngMaterial",
    "ngMessages",
    "ui.mask",
    "ui.router",
    "ui.bootstrap",
    "ui.select",
    "ngDialog",
    "restangular",
    "angular-loading-bar",
    "angular-ladda",
    "angularMoment",
    "blockUI",
    "angular-confirm",
    "oitozero.ngSweetAlert",
    "pascalprecht.translate",
    "toaster",
    "toggle-switch",
    "chieffancypants.loadingBar",
    "mainApp.constants.init",
    "mainApp.constant.strings",
    "mainApp.constants.templates",
    "mainApp.directive.ngFooterInfo",
    "mainApp.directive.ngTranslateLanguageSelect",
    "mainApp.factory.httpErrorResponseInterceptorFactory",
    "mainApp.factory.localeCustomService",
    "mainApp.factory.settings",
    "mainApp.factory.translateservice",
    "mainApp.filter.currencyLocale",
    "mainApp.filter.numberLocale",
    "mainApp.filter.percentageLocale",
    "mainApp.filter.inputBoolToText",
    "mainApp.controller.about",
    "mainApp.controller.contact",
    "mainApp.controller.home",
    "mainApp.controller.index",
    "mainApp.controller.start",
    "mainApp.provider.localeCustomProvider",
    "mainApp.service.main"
  ];

  export const funcConfigMain = ($provide: angular.auto.IProvideService, $httpProvider: angular.IHttpProvider, $translateProvider: angular.translate.ITranslateProvider, $locationProvider: angular.ILocationProvider, $stateProvider: core.StateProvider, localeInitProvider: cfg3.ApplicationMain.localeInit) => {
    return new cfg1.ApplicationMain.funcMain($provide, $httpProvider, $translateProvider, $locationProvider, $stateProvider, localeInitProvider);
  };

  export const funcRunMain = () => {
    return new cfg2.ApplicationMain.funcMain();
  };

  /* Inyección de dependencias. */
  funcConfigMain.$inject = [ "$provide", "$httpProvider", "$translateProvider", "$locationProvider", "$stateProvider", "localeInitProvider" ];
  funcRunMain.$inject = [];

  /* Definición principal de la aplicación de AngularJS. */
  angular.module("mainApp", _injectAll).config(ApplicationMain.funcConfigMain).run(ApplicationMain.funcRunMain);
}
