/* AngularJS */
import * as angular from "angular";
import * as core from "@uirouter/angularjs/lib/stateProvider";

import * as cfg2 from "../constants/app.constants.strings";
import * as cfg3 from "../providers/app.provider.init";
import * as cfg4 from "../constants/app.constants.templates";

export module ApplicationMain {
  /* Cargando el valor de "app.Constants". */
  const constantOne = cfg2.ApplicationMain.appStrings.Default;
  const constantTwo = cfg4.ApplicationMain.appTemplates.Default;

  /* Constante 'ExceptionHandler' para AngularJS. Tomado de: https://stackoverflow.com/questions/42765259/cannot-decorate-angularjs-exceptionhandler-using-typescript-as-a-function-isn */
  export const ExtendExceptionHandler = ($delegate: angular.IExceptionHandlerService) => {
    return function(exception: Error, cause?: string): void {
      $delegate(exception, cause);
    };
  };

  /* Inyectamos las dependencias. */
  ExtendExceptionHandler.$inject = [ "$delegate" ];

  /* Clase 'funcMain' para el arranque de AngularJS. Tomado de: https://pintoservice.wordpress.com/2015/08/24/create-angular-app-config-using-typescript-class/ */
  export class funcMain {
    httpProvObj: angular.IHttpProvider;
    provObj: angular.auto.IProvideService;
    translateObj: angular.translate.ITranslateProvider;
    locationObj: angular.ILocationProvider;
    stateObj: core.StateProvider;

    constructor(private $provide: angular.auto.IProvideService, private $httpProvider: angular.IHttpProvider, private $translateProvider: angular.translate.ITranslateProvider, private $locationProvider: angular.ILocationProvider, private $stateProvider: core.StateProvider, private localeInitProvider: cfg3.ApplicationMain.localeInit) {
      this.httpProvObj = $httpProvider;
      this.provObj = $provide;
      this.translateObj = $translateProvider;
      this.locationObj = $locationProvider;
      this.stateObj = $stateProvider;

      /* Inicializamos la clase. */
      this.init();

      /* Como es un objeto que se devuelve, lo devolvemos igual como clase. */
      return this;
    }

    private init(): void {
      this.provObj.decorator("$exceptionHandler", ExtendExceptionHandler);

      /* Interceptor. */
      this.httpProvObj.interceptors.push("httpErrorResponseInterceptorService");

      /* HTTP requests. */
      if (this.httpProvObj.defaults.headers?.get !== undefined) {
        this.httpProvObj.defaults.headers.get = {};

        /* Limpieza de encabezados, al iniciar la aplicación web. */
        this.httpProvObj.defaults.headers.get["Cache-Control"] = "no-cache";
        this.httpProvObj.defaults.headers.get["Pragma"] = "no-cache";
      }

      /* Activamos el modo HTML5. */
      this.locationObj.html5Mode({
        enabled: true,
        requireBase: true,
        rewriteLinks: true
      });

      this.stateObj.state("start", {
        url: "/",
        template: constantTwo.HTML_INDEX,
        controller: "startController",
        controllerAs: "vm"
      }).state("home", {
        url: "/home",
        template: constantTwo.HTML_HOME,
        controller: "homeController",
        controllerAs: "vm1"
      }).state("contact", {
        url: "/contact",
        template: constantTwo.HTML_CONTACT,
        controller: "contactController",
        controllerAs: "vm2"
      }).state("about", {
        url: "/about",
        template: constantTwo.HTML_ABOUT,
        controller: "aboutController",
        controllerAs: "vm3"
      });

      /* Translation => Configuración y carga de los strings de traducción de idiomas. */
      this.translateObj.useSanitizeValueStrategy("sanitizeParameters");
      this.translateObj.translations("es-MX", constantOne.collectionStrings.esMxStrings);
      this.translateObj.translations("en-US", constantOne.collectionStrings.enUsStrings);
      this.translateObj.translations("en-GB", constantOne.collectionStrings.enGbStrings);
      this.translateObj.preferredLanguage(constantOne.preferredLocale);

      /* Cuando se actualiza el navegador de internet, es importante preservar los cambios de traducción aplicados a la aplicación web. */
      this.translateObj.useLocalStorage(); // $translateProvider.useCookieStorage();

      /* Aquí uso mi proveedor. */
      this.localeInitProvider.setCurrentLocale(constantOne.preferredLanguage);
    }
  }
}
