/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Importando el servicio 'mainService' y el proveedor 'localeinit'. */
import * as prov1 from "../providers/app.provider.init";
import * as svc1 from "../services/app.service.main";

/* Módulo. */
export module ApplicationMain {

  /* Intefaz base para el interceptor. */
  export interface ILocaleCustomService {
    initLocale: () => void;
    setLocaleApply: (localeValue) => void;
  }

  /* Clase. => Tomado de: https://gist.github.com/adhamankar/0686edcca2aeb0fc8b38 */
  export class localeCustomService implements ILocaleCustomService {
    localeCfg: angular.ILocaleService;
    translateCfg: angular.translate.ITranslateService;
    localesValues: any;

    /* Inyección de dependencias. */
    static $inject: string[] = [ "$locale", "$translate", "localeInit", "mainService" ];

    /* Constructor de la clase. */
    constructor(private $locale: angular.ILocaleService, private $translate: angular.translate.ITranslateService, private localeInit: prov1.ApplicationMain.IInitService, private mainService: svc1.ApplicationMain.IMainService) {
      this.localeCfg = $locale;
      this.translateCfg = $translate;

      /* La variable "locales" es una colección de las configuraciones regionales de los países del mundo.
      Se pueden agregar las que sean necesarias, siempre y cuando, tambien las traducciones sean las mismas en
      los atributos "locales" y "collectionStrings" del objeto "appDataLanguageConstant".
      Fuente de plantillas para codificación del idioma: https://github.com/angular/bower-angular-i18n
      Fuente de los códigos de monedas de los países: https://www.techonthenet.com/js/currency_codes.php
      Fuente de las especificaciones de formatos de fecha para jQuery UI: https://github.com/jquery/jquery-ui/tree/master/ui/i18n
      Se agregó un atributo llamado "CURRENCY_CODE" debido a que es necesario para mostrar correctamente los valores monetarios de los países.
      Ese atributo no aparece en el primer enlace.
      */
      this.localesValues = {
        "es-MX": {
          "PLURAL_CATEGORY": { "ZERO": "zero", "ONE": "one", "TWO": "two", "FEW": "few", "MANY": "many", "OTHER": "other" },
          "DATETIME_FORMATS": {
            "AMPMS": [ "a. m.", "p. m." ],
            "DAY": [ "Domingo", "Lunes", "Martes", "Mi\u00e9rcoles", "Jueves", "Viernes", "S\u00e1bado" ],
            "ERANAMES": [ "antes de Cristo", "despu\u00e9s de Cristo" ],
            "ERAS": [ "a. C.", "d. C." ],
            "FIRSTDAYOFWEEK": 6,
            "MONTH": [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            "SHORTDAY": [ "Dom.", "Lun.", "Mar.", "Mi\u00e9.", "Jue.", "Vie.", "S\u00e1b." ],
            "SHORTMONTH": [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            "STANDALONEMONTH": [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            "WEEKENDRANGE": [ 5, 6 ],
            "fullDate": "EEEE, d 'de' MMMM 'de' y",
            "longDate": "d 'de' MMMM 'de' y",
            "medium": "dd/MM/y HH:mm:ss",
            "mediumDate": "dd/MM/y",
            "mediumTime": "HH:mm:ss",
            "short": "dd/MM/yy HH:mm",
            "shortDate": "dd/MM/yy",
            "shortTime": "HH:mm"
          },
          "NUMBER_FORMATS": {
            "CURRENCY_SYM": "\u0024",
            "CURRENCY_CODE": "MXN",
            "DECIMAL_SEP": ".",
            "GROUP_SEP": ",",
            "PATTERNS": [
              { "gSize": 3, "lgSize": 3, "maxFrac": 3, "minFrac": 0, "minInt": 1, "negPre": "-", "negSuf": "", "posPre": "", "posSuf": "" },
              { "gSize": 3, "lgSize": 3, "maxFrac": 2, "minFrac": 2, "minInt": 1, "negPre": "-\u00a4", "negSuf": "", "posPre": "\u00a4", "posSuf": "" }
            ]
          },
          "DATEPICKER_REGIONAL": {
            "closeText": "Cerrar",
            "prevText": "\u003c Anterior",
            "nextText": "Siguiente \u003e",
            "currentText": "Hoy",
            "monthNames": [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            "monthNamesShort": [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            "dayNames": [ "Domingo", "Lunes", "Martes", "Mi\u00e9rcoles", "Jueves", "Viernes", "S\u00e1bado" ],
            "dayNamesShort": [ "Dom", "Lun", "Mar", "Mi\u00e9", "Jue", "Vie", "S\u00e1b" ],
            "dayNamesMin": [ "D", "L", "M", "M", "J", "V", "S" ],
            "weekHeader": "Sm",
            "dateFormat": "dd/mm/yy",
            "firstDay": 1,
            "isRTL": false,
            "showMonthAfterYear": false,
            "yearSuffix": ""
          },
          "id": "es-MX",
          "localeID": "es_MX",
          "pluralCat": function(n: number): string { if (n === 1) { return "ONE"; } return "OTHER"; }
        },
        "en-US": {
          "PLURAL_CATEGORY": { "ZERO": "zero", "ONE": "one", "TWO": "two", "FEW": "few", "MANY": "many", "OTHER": "other" },
          "DATETIME_FORMATS": {
            "AMPMS": [ "AM", "PM" ],
            "DAY": [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
            "ERANAMES": [ "Before Christ", "Anno Domini" ],
            "ERAS": [ "BC", "AD" ],
            "FIRSTDAYOFWEEK": 6,
            "MONTH": [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            "SHORTDAY": [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
            "SHORTMONTH": [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
            "STANDALONEMONTH": [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            "WEEKENDRANGE": [ 5, 6 ],
            "fullDate": "EEEE, MMMM d, y",
            "longDate": "MMMM d, y",
            "medium": "MMM d, y h:mm:ss a",
            "mediumDate": "MMM d, y",
            "mediumTime": "h:mm:ss a",
            "short": "M/d/yy h:mm a",
            "shortDate": "M/d/yy",
            "shortTime": "h:mm a"
          },
          "NUMBER_FORMATS": {
            "CURRENCY_SYM": "\u0024",
            "CURRENCY_CODE": "USD",
            "DECIMAL_SEP": ".",
            "GROUP_SEP": ",",
            "PATTERNS": [
              { "gSize": 3, "lgSize": 3, "maxFrac": 3, "minFrac": 0, "minInt": 1, "negPre": "-", "negSuf": "", "posPre": "", "posSuf": "" },
              { "gSize": 3, "lgSize": 3, "maxFrac": 2, "minFrac": 2, "minInt": 1, "negPre": "-\u00a4", "negSuf": "", "posPre": "\u00a4", "posSuf": "" }
            ]
          },
          "DATEPICKER_REGIONAL": {
            "closeText": "Done",
            "prevText": "\u003c Previous",
            "nextText": "Next \u003e",
            "currentText": "Today",
            "monthNames": [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            "monthNamesShort": [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
            "dayNames": [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
            "dayNamesShort": [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
            "dayNamesMin": [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" ],
            "weekHeader": "Wk",
            "dateFormat": "dd/mm/yy",
            "firstDay": 1,
            "isRTL": false,
            "showMonthAfterYear": false,
            "yearSuffix": ""
          },
          "id": "en-US",
          "localeID": "en_US",
          "pluralCat": function(n: string, optPrecision: number): string { const i = +n | 0; const vf = mainService.getVF(n, optPrecision); if (i === 1 && vf.v === 0) { return "ONE"; } return "OTHER"; }
        },
        "en-GB": {
          "DATETIME_FORMATS": {
            "AMPMS": [ "am", "pm" ],
            "DAY": [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
            "ERANAMES": [ "Before Christ", "Anno Domini" ],
            "ERAS": [ "BC", "AD" ],
            "FIRSTDAYOFWEEK": 0,
            "MONTH": [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            "SHORTDAY": [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
            "SHORTMONTH": [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
            "STANDALONEMONTH": [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            "WEEKENDRANGE": [ 5, 6 ],
            "fullDate": "EEEE, d MMMM y",
            "longDate": "d MMMM y",
            "medium": "d MMM y HH:mm:ss",
            "mediumDate": "d MMM y",
            "mediumTime": "HH:mm:ss",
            "short": "dd/MM/y HH:mm",
            "shortDate": "dd/MM/y",
            "shortTime": "HH:mm"
          },
          "NUMBER_FORMATS": {
            "CURRENCY_SYM": "\u00a3",
            "CURRENCY_CODE": "GBP",
            "DECIMAL_SEP": ".",
            "GROUP_SEP": ",",
            "PATTERNS": [
              { "gSize": 3, "lgSize": 3, "maxFrac": 3, "minFrac": 0, "minInt": 1, "negPre": "-", "negSuf": "", "posPre": "", "posSuf": "" },
              { "gSize": 3, "lgSize": 3, "maxFrac": 2, "minFrac": 2, "minInt": 1, "negPre": "-\u00a4", "negSuf": "", "posPre": "\u00a4", "posSuf": "" }
            ]
          },
          "DATEPICKER_REGIONAL": {
            "closeText": "Done",
            "prevText": "\u003c Previous",
            "nextText": "Next \u003e",
            "currentText": "Today",
            "monthNames": [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            "monthNamesShort": [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
            "dayNames": [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
            "dayNamesShort": [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
            "dayNamesMin": [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" ],
            "weekHeader": "Wk",
            "dateFormat": "dd/mm/yy",
            "firstDay": 1,
            "isRTL": false,
            "showMonthAfterYear": false,
            "yearSuffix": ""
          },
          "id": "en-GB",
          "localeID": "en_GB",
          "pluralCat": function(n: string, optPrecision: number): string { const i = +n | 0; const vf = mainService.getVF(n, optPrecision); if (i === 1 && vf.v === 0) { return "ONE"; } return "OTHER"; }
        }
      };

      /* Como es un objeto que se devuelve, este factory lo devolvemos así mismo como factory. */
      return this;
    }

    /* Inicializar una variable. */
    initLocale = () => {
      angular.copy(this.localesValues[this.localeInit.getCurrentLocale()], this.localeCfg);
    }

    /* Asignar un idioma. */
    setLocaleApply = (localeValue: string) => {
      angular.copy(this.localesValues[localeValue], this.localeCfg);
    }
  }
}

/* Definición del modulo de AngularJS del tipo 'factory'. */
export default angular.module("mainApp.factory.localeCustomService", []).factory("localeCustomService", ApplicationMain.localeCustomService).name;
