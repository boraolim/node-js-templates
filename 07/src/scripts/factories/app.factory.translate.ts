/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Importando el servicio 'mainService' y el proveedor 'localeinit'. */
import * as const1 from "../constants/app.constants.strings";
import * as fact1 from "../factories/app.factory.localecustom";

/* Módulo. */
export module ApplicationMain {

  /* Clase que contiene la información de una lista de idiomas. */
  class LanguageList {
    keyLanguage: string;
    languageName: string;

    constructor(keyLang: string, langName: string) {
      this.keyLanguage = keyLang;
      this.languageName = langName;
      return this;
    }
  }

  /* Intefaz base para el interceptor. */
  export interface ITranslateFactory {
    checkLocaleIsValid: (localeString) => boolean;
    setLocale: (localeString) => void;
    getLocales: () => string[];
    getLocaleDisplayName: () => string;
    setLocalesByDisplayName: (localeDisplayName: string) => void;
    getLocalesDisplayNames: () => string[];
    getLocalesDisplayCollectionNames: () => LanguageList[];
  }

  /* Cargando el valor de "app.Strings". */
  const constant = const1.ApplicationMain.appStrings.Default;

  /* Clase. => Tomado de: https://gist.github.com/adhamankar/0686edcca2aeb0fc8b38 */
  export class translateService implements ITranslateFactory {
    translateCfg: angular.translate.ITranslateService;
    rootScopeCfg: angular.IRootScopeService;
    localeCustomSvc: fact1.ApplicationMain.ILocaleCustomService;
    currentLocale: string;
    localesObj: string[] = [];
    _LOCALES: string[] = [];
    _LOCALES_DISPLAY_NAMES: string[] = [];
    _LOCALES_DISPLAY_NAMES_ARRAYSTR: LanguageList[] = [];

    /* Inyección de dependencias. */
    static $inject: string[] = [ "$translate", "$rootScope", "localeCustomService" ];

    /* Constructor de la clase. */
    constructor(private $translate: angular.translate.ITranslateService, private $rootScope: angular.IRootScopeService, private localeCustomService: fact1.ApplicationMain.ILocaleCustomService) {
      this.translateCfg = $translate;
      this.rootScopeCfg = $rootScope;
      this.localesObj = constant.locales;
      this.localeCustomSvc = localeCustomService;

      /* Actualmente el idioma de la aplicación web. */
      this.currentLocale = this.translateCfg.proposedLanguage();

      /* Locales and locales display names */
      this._LOCALES = Object.keys(this.localesObj);
      if (!this._LOCALES || this._LOCALES.length === 0) {
        console.error("There are no _LOCALES provided");
      }

      /* Array string and object List. */
      for (const [ key, value ] of Object.entries(this.localesObj)) {
        this._LOCALES_DISPLAY_NAMES.push(value);
        this._LOCALES_DISPLAY_NAMES_ARRAYSTR.push({ "keyLanguage": key, "languageName": value });
      }

      /* EVENTS => On successful applying translations by angular-translate. */
      this.rootScopeCfg.$on("$translateChangeSuccess", function(event: any, data: any): void {
        /* Sets "lang" attribute to html */
        document.documentElement.setAttribute("lang", data.language);

        /* Asking angular-dynamic-locale to load and apply proper AngularJS $locale setting */
        localeCustomService.setLocaleApply(data.language.replace(/_/g, "-"));
      });

      /* Como es un objeto que se devuelve, este factory lo devolvemos así mismo como factory. */
      return this;
    }

    /* Validar si existe un idioma en la lista inicial. */
    checkLocaleIsValid = (localeString: string) => {
      return this._LOCALES.indexOf(localeString) !== -1;
    }

    /* Asignar un idioma existente. */
    setLocale = (localeString: string) => {
      if (!this.checkLocaleIsValid(localeString)) {
        console.error("Locale name '" + localeString + "' is invalid");
        return;
      }

      /* Updating current locale. */
      this.currentLocale = localeString;

      /* Asking angular-translate to load and apply proper translations. */
      this.translateCfg.use(localeString);
    }

    /* Mostrar la lista de idiomas disponibles. */
    getLocales = () => {
      return this.localesObj;
    }

    /* Mostrar el idioma seleccionado. */
    getLocaleDisplayName = () => {
      return this.localesObj[this.currentLocale];
    }

    /* Asignar el idioma por nombre. */
    setLocalesByDisplayName = (localeName: string) => {
      this.setLocale(this._LOCALES[this._LOCALES_DISPLAY_NAMES.indexOf(localeName)]);
    }

    /* Mostrar los idiomas existentes por nombre. */
    getLocalesDisplayNames = () => {
      return this._LOCALES_DISPLAY_NAMES;
    }

    getLocalesDisplayCollectionNames = () => {
      return this._LOCALES_DISPLAY_NAMES_ARRAYSTR;
    }
  }
}

/* Definición del modulo de AngularJS del tipo 'factory'. */
export default angular.module("mainApp.factory.translateservice", []).factory("translateService", ApplicationMain.translateService).name;
