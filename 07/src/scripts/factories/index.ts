import "./app.factory.interceptor";
import "./app.factory.localecustom";
import "./app.factory.settings";
import "./app.factory.translate";
