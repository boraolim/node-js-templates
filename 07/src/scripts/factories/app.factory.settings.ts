/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Importando las constantes de inicio 'constants'. */
import * as cs1 from "../constants/app.constants.init";

/* Módulo. */
export module ApplicationMain {

  export interface ISettingsFactory {
    appName: string;
    companyName: string;
    version: string;
    author: string;
    contact: string;
    urlCompany: string;
    currentYear: number;
    timeExecution: number;
    hostApp: string;
    hostPort: number;
    hostProtocol: string;
    fullDomain: string;
    absUrl: string;
    apiBaseAddress: string;
    fullApiBaseAddress: string;
    defaultLanguage: string;
  }

  /* Cargando el valor de "app.Constants". */
  const constant = cs1.ApplicationMain.appInit.Default;

  /* Clase. */
  export class settingsFactory implements ISettingsFactory {
    appName: string;
    companyName: string;
    version: string;
    author: string;
    contact: string;
    urlCompany: string;
    currentYear: number;
    timeExecution: number;
    hostApp: string;
    hostPort: number;
    hostProtocol: string;
    fullDomain: string;
    absUrl: string;
    apiBaseAddress: string;
    fullApiBaseAddress: string;
    defaultLanguage: string;

    /* Inyección de dependencias. */
    static $inject: string[] = [ "$location" ];

    /* Constructor de la clase. */
    constructor(private $location: angular.ILocationService) {
      this.appName = constant.appName;
      this.companyName = constant.companyName;
      this.version = constant.versionApp;
      this.author = constant.author;
      this.contact = constant.contact;
      this.urlCompany = constant.urlCompany;
      this.currentYear = constant.currentYear;
      this.timeExecution = 500;
      this.hostApp = $location.host();
      this.hostPort = $location.port();
      this.hostProtocol = $location.protocol();
      this.fullDomain = $location.absUrl.toString().replace($location.url.toString(), "");
      this.absUrl = $location.absUrl.toString().replace($location.url.toString(), "");
      this.apiBaseAddress = constant.apiBaseAddress;
      this.fullApiBaseAddress = $location.absUrl.toString().replace($location.url.toString(), "") + constant.apiBaseAddress;
      this.defaultLanguage = constant.defaultLanguage;

      /* Como es un objeto que se devuelve, este factory lo devolvemos así mismo como factory. */
      return this;
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.factory.settings", []).factory("settingsService", ApplicationMain.settingsFactory).name;
