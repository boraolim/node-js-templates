/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Módulo. */
export module ApplicationMain {

  /* Intefaz base para el interceptor. */
  export interface IInterceptor {
    request: Function;
    requestError: Function;
    response: Function;
    responseError: Function;
  }

  /* Clase. => Tomado de: https://gist.github.com/adhamankar/0686edcca2aeb0fc8b38 */
  export class interceptorFactory implements IInterceptor {
    /* Inyección de dependencias. */
    static $inject: string[] = [ "$q", "$location" ];

    /* Constructor de la clase. */
    constructor(private $q: angular.IQService, private $location: angular.ILocationService) {
      /* Como es un objeto que se devuelve, este factory lo devolvemos así mismo como factory. */
      return this;
    }

    public request = (requestSuccess: any): angular.IPromise<any> => {
      console.log("intercepting request");
      return requestSuccess;
    }

    public requestError = (requestFailure: any): angular.IPromise<any> => {
      console.log("requestError reported");
      return requestFailure;
    }

    public response = (responseSuccess: any): angular.IPromise<any> => {
      console.log("success response reported with status: " + responseSuccess.status);
      return responseSuccess;
    }

    public responseError = (responseFailure: any): angular.IPromise<any> => {
      // var apiPattern = /^(?:\/api\/)\S+/;
      // we need filter the config object to ensure only check our API response.
      if (responseFailure.config.url.indexOf("api") !== -1) {
        // here we modify the http status code to 400 indicates this should be treated as an error.
        responseFailure.status = 400;
        // reject this response will let the end point caller's error handler be called. also, you can
        // chain an responseError interceptor to handle this error.
      } else {
        switch (responseFailure.status) {
          case 401:
            this.$location.path("/");
            break;
          case 404:
            this.$location.path("/404");
            break;
          default:
            this.$location.path("/error");
            break;
        }
      }
      return this.$q.reject(responseFailure);
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.factory.httpErrorResponseInterceptorFactory", []).factory("httpErrorResponseInterceptorService", ApplicationMain.interceptorFactory).name;
