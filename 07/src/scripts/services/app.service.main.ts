/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Módulo. */
export module ApplicationMain {

  /* Interfaz. */
  export interface IMainService {
    getRandomString: (length, chars) => angular.IPromise<unknown>;
    getDecimals: (n) => number;
    getVF: (n, optPrecision) => any;
    executeAPI: (tipoMetodo, urlAPI, parametros, encabezado) => angular.IPromise<unknown>;
  }

  /* Clase. */
  export class mainService implements IMainService {
    httpObj: angular.IHttpService;
    qObj: angular.IQService;
    timeOutObj: angular.ITimeoutService;

    /* Inyección de dependencias. */
    static $inject: string[] = [ "$http", "$q", "$timeout" ];

    /* Constructor de la clase. */
    constructor(private $http: angular.IHttpService, private $q: angular.IQService, private $timeout: angular.ITimeoutService) {
      this.httpObj = $http;
      this.qObj = $q;
      this.timeOutObj = $timeout;

      /* Devolvemos entonces el objeto. */
      return this;
    }

    /* Mostrar una cadena de texto aleatoria. */
    getRandomString = (length: number, chars: string) => {
      const deferred = this.qObj.defer();
      this.timeOutObj(function() {
        let result = "";
        for (let i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
        deferred.resolve(result);
      }, 500);
      return deferred.promise;
    }

    /* Mostra el número de decimales de un número real. */
    getDecimals = (n: string) => {
      n = n + "";
      const i: number = n.indexOf(".");
      return (i === -1) ? 0 : n.length - i - 1;
    }

    /* Ajustar la precisión de decimales exactos. */
    getVF = (n: string, optPrecision: number) => {
      let v: number = optPrecision;
      const num: number = +n;

      if (undefined === v) {
        v = Math.min(this.getDecimals(n), 3);
      }

      const base: number = Math.pow(10, v);
      const f: number = ((num * base) | 0) % base;
      return { v: v, f: f };
    }

    /* Ejecutar una petición HTTP para llamadas a Web API. */
    executeAPI = (tipoMetodo: string, urlAPI: string, parametros: any, encabezado: any) => {
      return (this.httpObj({
        method: tipoMetodo,
        url: urlAPI,
        data: parametros,
        headers: encabezado
      }));
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.service.main", []).service("mainService", ApplicationMain.mainService).name;
