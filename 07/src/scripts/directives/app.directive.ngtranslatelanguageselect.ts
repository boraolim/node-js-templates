/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Importando el servicio 'mainService' y el proveedor 'localeinit'. */
import * as svc3 from "../factories/app.factory.translate";

/* Módulo. */
export module ApplicationMain {

  /* Controlador interno para la directiva de AngularJS. */
  export class ctrlDirective2 implements angular.IController {
    svcTrans: svc3.ApplicationMain.ITranslateFactory;
    currentLocalName: string;
    displayNames: string[];
    visible: boolean;

    /* Inyección de dependencias. */
    static $inject: string[] = [ "translateService" ];

    /* Constructor de la clase controller. */
    constructor(private translateService: svc3.ApplicationMain.ITranslateFactory) {
      this.svcTrans = translateService;
      this.currentLocalName = this.svcTrans.getLocaleDisplayName();
      this.displayNames = this.svcTrans.getLocalesDisplayNames();
      this.visible = (this.svcTrans.getLocalesDisplayCollectionNames() && this.svcTrans.getLocalesDisplayCollectionNames().length > 1);

      /* Como es un objeto que se devuelve, este factory lo devolvemos así mismo como factory. */
      return this;
    }

    currentLocalDisplayName = () => {
      return this.currentLocalName;
    }

    changeLanguage(localeValue: string): void {
      this.svcTrans.setLocalesByDisplayName(localeValue);
    }
  }

  /* Directiva. */
  export class ngTranslateLanguageSelect implements angular.IDirective {
    restrict = "EA";
    /* template = "<p>Hola a todos.  {{ drc2.currentLocalDisplayName() }} </p>"; */
template = "<li class='dropdown' uib-dropdown ng-if='drc2.visible'><a href='#' class='dropdown-toggle' uib-dropdown-toggle data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'><i class='icon ion-android-globe'></i>&nbsp;<span>Cambiar idioma: {{ drc2.currentLocalDisplayName() }}</span>&nbsp;<span class='caret'></span></a><ul class='dropdown-menu' uib-dropdown-menu role='menu'><li role='menuitem' ng-repeat='item in drc2.displayNames' ng-click='drc2.changeLanguage(item);$parent.open =!$parent.open'><a href='#'><i class='fa fa-check-circle blue-color'></i>&nbsp;{{item.name}}</a></li></ul></li>";
    controller = ctrlDirective2;
    controllerAs = "drc2";
    bindToController = true;

    /* Constructor de la directiva de AngularJS. */
    /* constructor() { } */

    /* Objeto estatico tipo 'Factory' para usar el patrón de diseño singletón. */
    static factory(): angular.IDirectiveFactory {
      const _directive = () => new ngTranslateLanguageSelect();
      return _directive;
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.directive.ngTranslateLanguageSelect", []).directive("ngTranslateLanguageSelect", ApplicationMain.ngTranslateLanguageSelect.factory()).name;
