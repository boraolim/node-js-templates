/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Importando el servicio 'mainService' y el proveedor 'localeinit'. */
import * as sets1 from "../factories/app.factory.settings";

/* Módulo. */
export module ApplicationMain {

  /* Controlador interno para la directiva de AngularJS. */
  class ctrlDirective implements angular.IController {
    appName: string;
    companyName: string;
    version: string;
    author: string;
    currentYear: number;
    urlcompany: string;

    /* Inyección de dependencias. */
    static $inject: string[] = [ "settingsService" ];

    /* Constructor de la clase controller. */
    constructor(private settingsService: sets1.ApplicationMain.ISettingsFactory) {
      this.appName = settingsService.appName;
      this.companyName = settingsService.companyName;
      this.version = settingsService.version;
      this.author = settingsService.author;
      this.currentYear = settingsService.currentYear;
      this.urlcompany = settingsService.urlCompany;

      /* Como es un objeto que se devuelve, este factory lo devolvemos así mismo como factory. */
      return this;
    }
  }

  /* Directiva. */
  export class ngFooterInfo implements angular.IDirective {
    restrict = "A";
    template = "<div class='pull-right hidden-xs'><b>{{ 'GLOBAL_MAIN_FOOTER_VERSION_NUMBER' | translate }} </b> {{ ::drc1.version }}</div><strong>Copyright &copy; {{ ::drc1.currentYear }} <a href='{{ ::drc1.urlcompany }}'>{{ ::drc1.companyName }}</a></strong> {{ 'GLOBAL_MAIN_FOOTER_ALLRIGHTS' | translate }}";
    controller = ctrlDirective;
    controllerAs = "drc1";
    bindToController = true;

    /* Constructor de la directiva de AngularJS. */
    /* constructor() { } */

    /* Objeto estatico tipo 'Factory' para usar el patrón de diseño singletón. */
    static factory(): angular.IDirectiveFactory {
      const _directive = () => new ngFooterInfo();
      return _directive;
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.directive.ngFooterInfo", []).directive("ngFooterInfo", ApplicationMain.ngFooterInfo.factory()).name;
