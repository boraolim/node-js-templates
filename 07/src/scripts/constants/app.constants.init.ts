/* Importando componentes de Node.js. */
import * as angular from "angular";

export module ApplicationMain {

  export class appInit {
    static get Default(): any {
      return {
        appName: "Comercios AMEX",
        companyName: "BzPay Solutions S.A. de C.V.",
        versionApp: "1.0.3.3456",
        author: "Olimpo Bonilla Ramírez",
        contact: "obonilla@bzpay.com.mx",
        urlCompany: "https://bzpay.com.mx/index.html",
        currentYear: new Date().getFullYear(),
        timeExecution: 500,
        defaultLanguage: "es-MX",
        apiBaseAddress: "api/v2/core"
      };
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.constants.init", []).constant("appInit", ApplicationMain.appInit.Default).name;
