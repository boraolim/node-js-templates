/* Importando componentes de Node.js. */
import * as angular from "angular";

export module ApplicationMain {

  export class appTemplates {
    static get Default(): any {
      return {
        "HTML_INDEX": "<section class='content-header'><h1 class='box-title'>Contacto</h1><ol class='breadcrumb'><li> <a href='#'> <i class='fa fa-dashboard'></i> Principal </a></li><li class='active'>Principal</li></ol> </section><section class='content'><div class='row'><div class='col-xs-12'><div class='box'><div class='box-header'><p class='box-title'>Contacto.</p></div><div class='box-body'><p>Esto es la p&aacute;gina de Acerca de...</p></div></div></div></div> </section>",
        "HTML_HOME": "<section class='content-header'><h1 class='box-title'>Contacto</h1><ol class='breadcrumb'><li> <a href='#'> <i class='fa fa-dashboard'></i> Inicio </a></li><li class='active'>Principal</li></ol> </section><section class='content'><div class='row'><div class='col-xs-12'><div class='box'><div class='box-header'><p class='box-title'>Contacto.</p></div><div class='box-body'><p>Esto es la p&aacute;gina de Acerca de...</p></div></div></div></div> </section>",
        "HTML_CONTACT": "<section class='content-header'><h1 class='box-title'>Contacto</h1><ol class='breadcrumb'><li> <a href='#'> <i class='fa fa-dashboard'></i> Contacto </a></li><li class='active'>Principal</li></ol> </section><section class='content'><div class='row'><div class='col-xs-12'><div class='box'><div class='box-header'><p class='box-title'>Contacto.</p></div><div class='box-body'><p>Esto es la p&aacute;gina de Acerca de...</p></div></div></div></div> </section>",
        "HTML_ABOUT": "<section class='content-header'><h1 class='box-title'>Contacto</h1><ol class='breadcrumb'><li> <a href='#'> <i class='fa fa-dashboard'></i> Acerca de... </a></li><li class='active'>Principal</li></ol> </section><section class='content'><div class='row'><div class='col-xs-12'><div class='box'><div class='box-header'><p class='box-title'>Contacto.</p></div><div class='box-body'><p>Esto es la p&aacute;gina de Acerca de...</p></div></div></div></div> </section>"
      };
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.constants.templates", []).constant("appTemplates", ApplicationMain.appTemplates.Default).name;
