/* Importando componentes de Node.js. */
import * as angular from "angular";

export module ApplicationMain {

  /* Clase del controlador. */
  export class startController implements angular.IController {
    /* Inyección de dependencias. */
    static $inject = [];

    /* Constructor de la clase. */
    constructor() {
      /* Devolvemos entonces el objeto. */
      return this;
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.controller.start", []).controller("startController", ApplicationMain.startController);
