/* Importando componentes de Node.js. */
import * as angular from "angular";

export module ApplicationMain {

  /* Clase del controlador. */
  export class contactController implements angular.IController {
    /* Inyección de dependencias. */
    static $inject = [];

    /* Constructor de la clase. */
    constructor() {
      /* Devolvemos entonces el objeto. */
      return this;
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.controller.contact", []).controller("contactController", ApplicationMain.contactController);
