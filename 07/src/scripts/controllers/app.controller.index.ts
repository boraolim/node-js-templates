/* Importando componentes de Node.js. */
import * as angular from "angular";
import "jquery";
import * as core from "@uirouter/core";

/* Importando el servicio 'mainService'. */
import * as svc1 from "../services/app.service.main";
import * as svc2 from "../factories/app.factory.settings";
import * as svc3 from "../factories/app.factory.translate";

export module ApplicationMain {

  /* Clase del controlador. */
  export class indexController implements angular.IController {
    greeting: string;
    stateMenu: core.UIRouterGlobals;
    svcMain: svc1.ApplicationMain.IMainService;
    svcSettings: svc2.ApplicationMain.ISettingsFactory;
    svcTrans: svc3.ApplicationMain.ITranslateFactory;
    applicationName: string;
    puerto: number;
    listaIdiomas: any[];
    idiomaLocal: boolean;
    idiomaLocal2: string;
    contenido: any[];

    /* Inyección de dependencias. */
    static $inject = [ "$state", "mainService", "settingsService", "translateService" ];

    /* Constructor de la clase. */
    constructor(private $state: core.UIRouterGlobals, private mainService: svc1.ApplicationMain.IMainService, private settingsService: svc2.ApplicationMain.ISettingsFactory, private translateService: svc3.ApplicationMain.ITranslateFactory) {
      this.greeting = "";
      this.svcMain = mainService;
      this.svcSettings = settingsService;
      this.stateMenu = $state;
      this.svcTrans = translateService;
      this.listaIdiomas = this.svcTrans.getLocales();
      this.idiomaLocal = this.svcTrans.checkLocaleIsValid("es-MX");
      this.idiomaLocal2 = this.svcTrans.getLocaleDisplayName();
      this.contenido = [];
      this.applicationName = settingsService.appName;
      this.puerto = settingsService.hostPort;

      /* Inicializamos una función de la clase. */
      this.disableEnvironment();

      /* Devolvemos entonces el objeto. */
      return this;
    }

    disableEnvironment(): void {
      // To disable right click
      document.addEventListener("contextmenu", event => event.preventDefault());

      // To disable F12 options
      document.onkeypress = function(event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
          /* notificationService.showErrorMessage("Sorry, This Functionality Has Been Disabled!", "Error"); */
          window.alert("Sorry, This Functionality Has Been Disabled!");
          return false;
        }
      };

      // Attaching the event to the document is better
      $(document).on("mousedown", function(event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
          /* notificationService.showErrorMessage("Sorry, This Functionality Has Been Disabled!", "Error"); */
          window.alert("Sorry, This Functionality Has Been Disabled!");
          return false;
        }
      });

      document.onkeydown = function(event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
          /* notificationService.showErrorMessage("Sorry, This Functionality Has Been Disabled!", "Error"); */
          window.alert("Sorry, This Functionality Has Been Disabled!");
          return false;
        }
      };

      // To disable ctrl+c, ctrl+u, ctrl + shift + i and ctrl + shift + j
      $(document).ready(function($) {
        $(document).keydown(function(event) {
          const pressedKey = String.fromCharCode(event.keyCode).toLowerCase();

          if (event.ctrlKey && (pressedKey === "c" || pressedKey === "u" || pressedKey === "x")) {
            /* notificationService.showErrorMessage("Sorry, This Functionality Has Been Disabled!", "Error"); */
            window.alert("Sorry, This Functionality Has Been Disabled!");
            return false;
          } else if ((event.ctrlKey && event.shiftKey && event.keyCode === 73) || (event.ctrlKey && event.shiftKey && event.keyCode === 74)) {
            window.alert("Sorry, This Functionality Has Been Disabled!");
            /* notificationService.showErrorMessage("Sorry, This Functionality Has Been Disabled!", "Error"); */
            return false;
          }
        });
      });
    }

    /* Mostrar el status de la ruta, si esta activa. */
    getClass(path: string): string {
      console.log(this.stateMenu.current.name?.trim());
      if (this.stateMenu.current.name?.substr(0, path.length) === path) {
        return "active";
      } else {
        return "";
      }
    }

    /* Método de la clase. */
    getGreet(name: string): void {
      window.alert("Hola " + name.trim() + " " + this.svcMain.getDecimals("2354.39455") + " " + JSON.stringify(this.svcMain.getVF("2354.39855", 2)));
    }

    /* Mostrar cadena aleatoria. */
    getRandomString(): void {
      this.svcMain.getRandomString(15, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").then((states) => {
        this.greeting = <string>states;
        window.alert("Cadena de texto aleatoria generada: " + this.greeting.trim());
      });
    }

    /* Ejecutar una web api externa. */
    getExecuteAPI(): void {
      const promise: angular.IPromise<unknown> = this.svcMain.executeAPI("POST", "https://api-sepomex.hckdrk.mx/query/info_cp/09700", {}, {});

      promise.then((response: any) => {
        // Satisfactorio...
        if (response && response.status === 200) {
          this.contenido = JSON.parse(JSON.stringify(response.data));
        } else {
          this.contenido = [];
        }
      }, (msg: any) => {
        // Error
        window.alert(msg);
      });
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.controller.index", []).controller("indexController", ApplicationMain.indexController);
