/* Importando componentes de Node.js. */
import * as angular from "angular";

export module ApplicationMain {

  /* Clase del controlador. */
  export class homeController implements angular.IController {
    /* Inyección de dependencias. */
    static $inject = [];

    /* Constructor de la clase. */
    constructor() {
      /* Devolvemos entonces el objeto. */
      return this;
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.controller.home", []).controller("homeController", ApplicationMain.homeController);
