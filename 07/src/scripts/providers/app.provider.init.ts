/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Módulo. */
export module ApplicationMain {

  /* Interfaz de inicialización. */
  export interface IInitService {
    getCurrentLocale(): string;
  }

  /* Clase. Tomado de: http://www.software-architects.com/devblog/2014/10/24/AngularJS-Provider-in-TypeScript */
  export class localeInit implements angular.IServiceProvider {
    private _currentLocale = "";

    public setCurrentLocale(localeValue: string): void {
      this._currentLocale = localeValue;
    }

    /* Función tipo 'Factory' para el proveedor. */
    public $get() : IInitService {
      return {
        getCurrentLocale: () => { return this._currentLocale; }
      };
    }
  }
}

/* Exportamos entonces el controlador a AngularJS. */
export default angular.module("mainApp.provider.localeCustomProvider", []).provider("localeInit", ApplicationMain.localeInit).name;
