/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Módulo. */
export module ApplicationMain {

  export class numberLocale {
    /* Inyección de dependencias. */
    static $inject: string[] = [ "$locale" ];

    public static factory($locale) {
      function _numberLocale(input: any) {
        const localeId = $locale.id;
        const formats = $locale.NUMBER_FORMATS;
        const returnValue = Number(parseFloat(input)).toLocaleString(localeId, {
          style: "decimal",
          useGrouping: true,
          minimumFractionDigits: parseInt(formats.PATTERNS[0].minFrac),
          maximumFractionDigits: parseInt(formats.PATTERNS[0].maxFrac)
        });
        return returnValue;
      }
      _numberLocale.$stateful = true;
      return _numberLocale;
    }
  }
}

/* Definición del modulo de AngularJS del tipo 'filter'. */
export default angular.module("mainApp.filter.numberLocale", []).filter("numberLocale", ApplicationMain.numberLocale.factory).name;
