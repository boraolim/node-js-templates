/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Módulo. */
export module ApplicationMain {

  export class percentageLocale {
    /* Inyección de dependencias. */
    static $inject: string[] = [ "$locale" ];

    public static factory($locale) {
      function _percentageLocale(input: any) {
        const localeId = $locale.id;
        const formats = $locale.NUMBER_FORMATS;
        const returnValue = Number(parseFloat(input)).toLocaleString(localeId, {
          style: "percent",
          useGrouping: true,
          minimumFractionDigits: parseInt(formats.PATTERNS[0].minFrac),
          maximumFractionDigits: parseInt(formats.PATTERNS[0].maxFrac)
        });
        return returnValue;
      }

      _percentageLocale.$stateful = true;
      return _percentageLocale;
    }
  }
}

/* Definición del modulo de AngularJS del tipo 'filter'. */
export default angular.module("mainApp.filter.percentageLocale", []).filter("percentageLocale", ApplicationMain.percentageLocale.factory).name;
