/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Módulo. */
export module ApplicationMain {

  export class currencyLocale {
    /* Inyección de dependencias. */
    static $inject: string[] = [ "$locale" ];

    public static factory($locale) {
      function _currencyLocale(input: any) {
        const localeId = $locale.id;
        const formats = $locale.NUMBER_FORMATS;
        const returnValue = Number(parseFloat(input)).toLocaleString(localeId, {
          style: "currency",
          useGrouping: true,
          currencyDisplay: "symbol",
          currency: formats.CURRENCY_CODE,
          minimumFractionDigits: parseInt(formats.PATTERNS[0].minFrac),
          maximumFractionDigits: parseInt(formats.PATTERNS[0].maxFrac)
        });
        return returnValue;
      }

      _currencyLocale.$stateful = true;
      return _currencyLocale;
    }
  }
}

/* Definición del modulo de AngularJS del tipo 'filter'. */
export default angular.module("mainApp.filter.currencyLocale", []).filter("currencyLocale", ApplicationMain.currencyLocale.factory).name;
