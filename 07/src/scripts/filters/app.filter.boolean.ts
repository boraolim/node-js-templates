/* Importando componentes de Node.js. */
import * as angular from "angular";

/* Módulo. */
export module ApplicationMain {

  /* Función que se exporta como objeto de AngularJS Filter. */
  export function inputBoolToText() {
    function _inputBoolToText(input: boolean) {
      switch (input) {
        case false:
          return "NO";
        default:
          return "SI";
      }
    }

    _inputBoolToText.$stateful = true;
    return _inputBoolToText;
  }
}

/* Definición del modulo de AngularJS del tipo 'filter'. */
export default angular.module("mainApp.filter.inputBoolToText", []).filter("inputBoolToText", ApplicationMain.inputBoolToText).name;
