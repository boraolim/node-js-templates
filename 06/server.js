// Configuración de Express.js para Node.js
// Autor: Olimpo Bonilla Ramírez.
// Fecha: 2020-05-25.

var express = require("express");
var app = express();

app.set("port", (process.env.PORT || 8000));

app.use(express.static(__dirname + "/dist"));
// app.use("/public", express.static(__dirname + "/dist"));

/* other routes defined before catch-all */
/*
app.get("/home", (req, res) => {
  res.send('ok')
});

// The 404 Route (ALWAYS Keep this as the last route)
app.get('/404', function (req, res) {
  res.status(404);
});
*/

app.use(function(req, res, next) {
  if (req.path !== "/")
    return res.redirect("/");
  next();
});

app.get("/", function(req, res, next) {
  return res.sendFile("/dist/index.html", { root: __dirname + "/dist/index.html" });
});

app.listen(app.get("port"), function () {
    console.log("¡SATISFACTORIO! La aplicación se esta corriendo en el puerto ", app.get("port"));
});

// Fin del script.