"use strict";

const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
// const TerserPlugin = require("terser-webpack-plugin");

const entryPath = ["@babel/polyfill", "./src/app.js"];
const basePath = __dirname;
const distPath = "../dist";
const publicPathProd = "/";

module.exports = {
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".css", ".scss", ".sass", ".css", ".hbs", ".handlebars"]
  },
  entry: entryPath,
  output: {
    path: path.join(basePath, distPath),
    filename: "js/bundle.js"
  },
  performance: {
    hints: false,
    maxEntrypointSize: 1512000,
    maxAssetSize: 1512000
  },
  module: {
    rules: [{
        test: /(\.js|.jsx)$/i,
        exclude: /(node_modules|bower_components)/,
        use: [{
            loader: "babel-loader"
          },
          "eslint-loader"
        ]
      },
      {
        test: /(\.ts|.tsx)$/i,
        exclude: /(node_modules|bower_components)/,
        use: ["ts-loader"]
      },
      {
        test: /\.html$/,
        exclude: /(node_modules|bower_components)/,
        use: [{
          loader: "raw-loader"
        }]
      },
      {
        test: /\.(handlebars|hbs)$/i,
        use: [{
          loader: "handlebars-loader"
        }]
      },
      {
        test: /\.(sa|sc|c)ss$/i,
        exclude: /(node_modules|bower_components)/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              autoprefixer: {
                browser: ["last 2 versions"]
              },
              plugins: () => [autoprefixer]
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.(jpg|jpeg|png|gif|webp)$/i,
        use: [{
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "static/",
              useRelativePath: true
            }
          },
          {
            loader: "image-webpack-loader",
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              // optipng.enabled: false will disable optipng
              optipng: {
                enabled: false
              },
              pngquant: {
                quality: [0.65, 0.90],
                speed: 4
              },
              gifsicle: {
                interlaced: false,
              },
              // the webp option will enable WEBP
              webp: {
                quality: 75
              }
            }
          }
        ]
      },
      /*
      {
        test: /\.(eot|otf|svg|ttf|woff|woff2)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          publicPath: "../fonts",
          useRelativePath: true
        }
      } */
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          publicPath: "../fonts",
          mimeType: "application/octet-stream",
          useRelativePath: true
        }
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      },
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      },
      {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      },
      {
        test: /\.otf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          publicPath: "../fonts",
          mimeType: "image/svg+xml",
          useRelativePath: true
        }
      }
    ]
  },
  optimization: {
    noEmitOnErrors: true,
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions: {
          compress: true,
          ecma: 6,
          mangle: true,
          output: {
            comments: false,
          },
        },
        sourceMap: false
      })
    ]
  },
  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new HtmlWebpackPlugin({
      hash: true,
      inject: true,
      favicon: "./src/favicon.ico",
      template: "./src/index.handlebars",
      title: "ui-rooter",
      base: {
        "href": (process.env.NODE_ENV === "production") ? publicPathProd : "/"
      },
      publicPath: (process.env.NODE_ENV === "production") ? publicPathProd : "/",
      minify: {
        html5: true,
        compress: true,
        caseSensitive: true,
        removeComments: true,
        useShortDoctype: true,
        collapseWhitespace: true,
        removeEmptyElements: false,
        removeAttributeQuotes: false,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    }),
    new MiniCssExtractPlugin({
      filename: "css/[name]-styles.css",
      chunkFilename: "[id].css",
      compress: true
    }),
    new webpack.DefinePlugin({
      "IsProductive": (process.env.NODE_ENV === "production") ? true : false,
      "API_URL": (process.env.NODE_ENV === "production") ? JSON.stringify("http://produccion.com") : JSON.stringify("http://desarrollo.com"),
      "SERVICE_URL": (process.env.NODE_ENV === "production") ? JSON.stringify("http://produccion.com") : JSON.stringify("http://desarrollo.com")
    })
  ]
};
