config.$inject = [ "$httpProvider", "$urlRouterProvider", "$locationProvider" ];

export default function config($httpProvider, $urlRouterProvider, $locationProvider) {
  $httpProvider.interceptors.push("httpErrorResponseInterceptor");
  $locationProvider.html5Mode({
    enabled : true,
    requireBase : true,
    rewriteLinks : true
  });
  $urlRouterProvider.when("", "/home");
  $urlRouterProvider.when("/", "/home");
  $urlRouterProvider.otherwise("/404");
}
