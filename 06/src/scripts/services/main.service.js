import angular from "angular";

class mainService {
  // Constructor.
  constructor($http, $q) {
    this.$http = $http;
    this.$q = $q;
    this.names = [ "Cruz Azul", "América", "Chivas", "Pumas", "Barcelona FC", "Real Madrid", "Toluca FC", "Puebla", "León", "Xolos de Tijuana" ];
  }

  // Función que muestra el nombre de manera aleatoria.
  getName() {
    const totalNames = this.names.length;
    const rand = Math.floor(Math.random() * totalNames);
    return this.names[rand];
  }

  getAPI(typeMethod, urlAPI, params, headers) {
    return (this.$http({
      method : typeMethod,
      url : urlAPI,
      params : params,
      headers : headers
    }));
  }
}

mainService.$inject = [ "$http", "$q" ];

// Exportamos el servicio.
export default angular.module("mainApp.services.mainService", []).service("mainService", mainService).name;
