import angular from "angular";

// Definición de la funcionalidad de la directiva de AngularJS.
function about() {
  // Variable del tipo objeto.
  var directiveObj = {
    template : "<hr><p>&copy; <strong>{{company}}</strong> M&eacute;xico {{annio}}. Versi&oacute;n: <strong>{{version}}</strong>. Derechos Reservados.</p>",
    restrict : "E",
    scope : {
      autor : "=",
      version : "=",
      annio : "=",
      company : "="
    },
    controller : function($scope, $location, $filter) {
      // Nada por el momento.
    },
    link : function($scope, $element, $attrs) {
      // Nada por el momento.
    }
  };

  return directiveObj;
}

// Exportamos la directiva final.
export default angular.module("mainApp.directives.about", []).directive("about", about).name;
