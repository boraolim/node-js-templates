import angular from "angular";

// Definición de la funcionalidad de la directiva de AngularJS.
function greeting() {
  // Variable del tipo objeto.
  var directiveObj = {
    template : "<h1>Hola {{nombre}}</div>",
    restrict : "E",
    scope : {
      nombre : "="
    },
    controller : function($scope, $location, $filter) {
      // Nada por el momento.
    },
    link : function($scope, $element, $attrs) {
      // Nada por el momento.
    }
  };

  return directiveObj;
}

// Exportamos la directiva final.
export default angular.module("mainApp.directives.greeting", []).directive("greeting", greeting).name;
