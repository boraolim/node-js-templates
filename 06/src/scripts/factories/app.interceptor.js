import angular from "angular";

httpErrorResponseInterceptor.$inject = [ "$q", "$location", "appAlerts" ];

function httpErrorResponseInterceptor($q, $location, appAlerts) {
  var _alert = appAlerts;

  var interceptor = {
    response : function(responseData) {
      return responseData;
    },
    responseError : function error(response) {
      // var apiPattern = /^(?:\/api\/)\S+/;
      // we need filter the config object to ensure only check our API response.
      if (response.config.url.indexOf("api") !== -1) {
        // here we modify the http status code to 400 indicates this should be treated as an error.
        response.status = 400;
        _alert.add("danger", "Ocurrió un error interno en la llamada de API");
        console.log(_alert.get());
        
        // reject this response will let the end point caller's error handler be called. also, you can
        // chain an responseError interceptor to handle this error.
      } else {
        switch (response.status) {
          case 401:
            $location.path("/");
            break;
          case 404:
            $location.path("/404");
            break;
          default:
            $location.path("/error");
            break;
        }
      }

      return $q.reject(response);
    }
  };

  // Devuelvo el valor de la configuración.
  return interceptor;
}

export default angular.module("mainApp.httpErrorResponseInterceptor", []).factory("httpErrorResponseInterceptor", httpErrorResponseInterceptor).name;
