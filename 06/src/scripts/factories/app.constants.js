import angular from "angular";

function appConstants() {
  var settings = {
    "APPNAME" : "Aplicación de AngularJS",
    "COMPANY" : "Hogar S.A. de C.V.",
    "VERSION" : "1.0.0.0",
    "AUTHOR" : "Olimpo Bonilla Ramírez",
    "CONTACT" : "boraolim@gmail.com",
    "CURRENTYEAR" : new Date().getFullYear()
  };

  // Devuelvo el valor de la configuración.
  return settings;
}

export default angular.module("mainApp.appConstants", []).factory("appConstants", appConstants).name;
