import angular from "angular";

appAlerts.$inject = [ "$timeout" ];

function appAlerts($timeout) {
  var ALERT_TIMEOUT = 5000;
  var alerts = [];
  var settings = {
    add : add,
    closeAlert : closeAlert,
    closeAlertIdx : closeAlertIdx,
    clear : clear,
    get : get
  };

  // Agregar alerta.
  function add(type, msg, timeout) {
    if (timeout) {
      $timeout(function() { closeAlert(this); }, timeout);
    } else {
      $timeout(function() { closeAlert(this); }, ALERT_TIMEOUT);
    }

    return alerts.push({ type : type, msg : msg, close : function() { return closeAlert(this); } });
  }

  // Cerrar alerta.
  function closeAlert(alert) {
    return closeAlertIdx(alerts.indexOf(alert));
  }

  // Cerrar alerta por Id.
  function closeAlertIdx(index) {
    return alerts.splice(index, 1);
  }

  // Limpiar alertas.
  function clear() {
    alerts = [];
  }

  // Mostrar todas las alertas.
  function get() {
    console.log("Mostrando..." + alerts);
    return alerts;
  }

  // Devuelvo el valor de la configuración.
  return settings;
}

export default angular.module("mainApp.appAlerts", []).factory("appAlerts", appAlerts).name;
