import angular from "angular";

function appSettings() {
  var settings = {
    "ISPRODUCTIONVERSION" : IsProductive,
    "URLBASE" : API_URL + "/",
    "URLAUTH" : API_URL + "/authorization"
  };

  // Devuelvo el valor de la configuración.
  return settings;
}

export default angular.module("mainApp.appSettings", []).factory("appSettings", appSettings).name;
