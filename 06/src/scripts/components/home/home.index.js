/* Importando componentes de Node.js. */
import angular from "angular";
import ngRoute from "angular-ui-router";

/* Importando elementos del componente interno. */
import routing from "./home.routes";
import HomeController from "./home.controller";

/* Inyectando objetos al controlador. */
HomeController.$inject = [ "appSettings" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.home", [ ngRoute ]).config(routing).controller("HomeController", HomeController).name;
