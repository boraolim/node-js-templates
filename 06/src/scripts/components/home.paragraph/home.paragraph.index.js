/* Importando componentes de Node.js. */
import angular from "angular";
import ngRoute from "angular-ui-router";

/* Importando elementos del componente interno. */
import routing from "./home.paragraph.routes";
import HomeParagraphController from "./home.paragraph.controller";

/* Inyectando objetos al controlador. */
HomeParagraphController.$inject = [ "appSettings" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.home.paragraph", [ ngRoute ]).config(routing).controller("HomeParagraphController", HomeParagraphController).name;
