routes.$inject = [ "$stateProvider" ];

export default function routes($stateProvider) {
  $stateProvider.state("home.paragraph", {
    url : "/paragraph",
    template : "<h1>Parrafo...</h1>",
    controller : "HomeListController",
    controllerAs : "vm"
  });
}
