/* Importando componentes de Node.js. */
import angular from "angular";
import uirouter from "angular-ui-router";

/* Importando estilos CSS3 */

/* Importando elementos del componente interno. */
import routing from "./testapi.routes";
import TestAPIController from "./testapi.controller";

/* Inyectando objetos al controlador. */
TestAPIController.$inject = [ "appSettings", "appConstants", "mainService", "$q" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.testapi", [ uirouter ]).config(routing).controller("TestAPIController", TestAPIController).name;
