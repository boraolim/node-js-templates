routes.$inject = [ "$stateProvider" ];

export default function routes($stateProvider) {
  $stateProvider.state("testapi", {
    url : "/testapi",
    template : require("./testapi.handlebars"),
    controller : "TestAPIController",
    controllerAs : "vm"
  });
}
