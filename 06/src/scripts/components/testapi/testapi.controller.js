export default class TestAPIController {
  // Constructor.
  constructor(appSettings, appConstants, mainService) {
    this.nombre = "usuario";
    this.succeded = undefined;
    this.errorMessage = undefined;
    this.afirmacion = (Math.random() >= 0.5);
    this.contenido = [];
    this.CP = undefined;
    this.APIURL = appSettings.URLBASE;
    this.APIUR2 = appSettings.URLAUTH;
    this.IsProductive = appSettings.ISPRODUCTIONVERSION;
    this.dataLoading = false;

    // Servicios y componentes.
    this._mainService = mainService;
    this._appConstants = appConstants;
  }

  // Mostrar nombre de manera aleatoria: https://carlosazaustre.es/desarrollo-por-componentes-con-angular-1-5-con-es6-es2015
  // https://www.toptal.com/java/building-modern-web-applications-with-angularjs-and-play-framework
  // https://www.sitepoint.com/writing-angularjs-apps-using-es6/
  // https://www.c-sharpcorner.com/UploadFile/dev4634/understanding-http-interceptors-in-angular-js/
  // https://www.codeproject.com/Articles/4091163/AngularJS-ngResource-Tutorial
  getCPByAPI(id) {
    this.dataLoading = true; console.clear();

    var promise = this._mainService.getAPI("POST", "https://api-sepomex.hckdrk.mx/query/info_cp/" + id, {}, {});

    promise.then(response => {
      // Satisfactorio...
      if (response) {
        this.succeded = true;
        this.contenido = JSON.parse(JSON.stringify(response.data, null, 3));
      } else {
        this.succeded = false; this.contenido = [];
        this.errorMessage = "Ocurrió un error al ejecutar esta acción";
      }
    }, msg => {
      // Error
      this.succeded = false; this.contenido = [];
      this.errorMessage = "Ocurrió un error al ejecutar esta acción";
    });

    this.dataLoading = false;
  }
}
