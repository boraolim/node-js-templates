/* Importando componentes de Node.js. */
import angular from "angular";
import ngRoute from "angular-ui-router";

/* Importando elementos del componente interno. */
import routing from "./404.routes";
import NotFoundController from "./404.controller";

/* Inyectando objetos al controlador. */
NotFoundController.$inject = [ "appSettings", "$location" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.notfound", [ ngRoute ]).config(routing).controller("NotFoundController", NotFoundController).name;
