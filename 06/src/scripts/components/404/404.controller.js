export default class NotFoundController {
  // Constructor.
  constructor($appSettings, $location) {
    this.nombre = "usuario";
    this.locationCtrl = $location;
    this.afirmacion = (Math.random() >= 0.5);
    this.arregloUno = [ "foo", "bar", "baz" ];
    this.APIURL = $appSettings.URLBASE;
    this.APIUR2 = $appSettings.URLAUTH;
    this.IsProductive = $appSettings.ISPRODUCTIONVERSION;
  }

  // Regresar a la página anterior.
  backHistory() {
    window.history.back();
  }

  // Regresar a página de inicio.
  backHome() {
    this.locationCtrl.path("/home");
  }
}
