/* Importando componentes de Node.js. */
import angular from "angular";
import uirouter from "angular-ui-router";

/* Importando estilos CSS3 */

/* Importando elementos del componente interno. */
import routing from "./contact.routes";
import ContactController from "./contact.controller";

/* Inyectando objetos al controlador. */
ContactController.$inject = [ "appSettings", "appConstants", "mainService" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.contact", [ uirouter ]).config(routing).controller("ContactController", ContactController).name;
