routes.$inject = [ "$stateProvider" ];

export default function routes($stateProvider) {
  $stateProvider.state("contact", {
    url : "/contact",
    template : require("./contact.handlebars"),
    controller : "ContactController",
    controllerAs : "vm"
  });
}
