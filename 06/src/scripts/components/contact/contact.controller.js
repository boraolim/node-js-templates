export default class ContactController {
  // Constructor.
  constructor(appSettings, appConstants, mainService) {
    this.nombre = "usuario";
    this.afirmacion = (Math.random() >= 0.5);
    this.contenido = [];
    this.statuscode = 0;
    this.statustext = "";
    this.CP = 0;
    this.APIURL = appSettings.URLBASE;
    this.APIUR2 = appSettings.URLAUTH;
    this.IsProductive = appSettings.ISPRODUCTIONVERSION;

    // Servicios y componentes.
    this._mainService = mainService;
    this._appConstants = appConstants;
  }

  changeName() {
    this.nombre = "angular-tips";
    window.alert("Estatus: " + this.IsProductive + "\nDirección URL: " + this.APIURL + "\nDirección URL de autenticación: " + this.APIUR2);
  }

  // Mostrar nombre de manera aleatoria.
  randomName() {
    this.nombre = this._mainService.getName();
  }

  randomBool() {
    this.afirmacion = (Math.random() >= 0.5);
  }
}
