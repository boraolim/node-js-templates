/* Importando componentes de Node.js. */
import angular from "angular";
import ngRoute from "angular-ui-router";

/* Importando elementos del componente interno. */
import routing from "./home.list.routes";
import HomeListController from "./home.list.controller";

/* Inyectando objetos al controlador. */
HomeListController.$inject = [ "appSettings" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.home.list", [ ngRoute ]).config(routing).controller("HomeListController", HomeListController).name;
