routes.$inject = [ "$stateProvider" ];

export default function routes($stateProvider) {
  $stateProvider.state("home.list", {
    url : "/list",
    template : require("./home.list.handlebars"),
    controller : "HomeListController",
    controllerAs : "vm"
  });
}
