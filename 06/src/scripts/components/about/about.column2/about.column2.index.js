/* Importando componentes de Node.js. */
import angular from "angular";

/* Importando elementos del componente interno. */
import AboutColumn2Controller from "./about.column2.controller";

/* Inyectando objetos al controlador. */
AboutColumn2Controller.$inject = [ "appSettings" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.about.column2", []).controller("AboutColumn2Controller", AboutColumn2Controller).name;
