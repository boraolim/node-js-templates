export default class AboutColumn2Controller {
  // Constructor.
  constructor($appSettings) {
    this.nombre = "usuario";
    this.afirmacion = (Math.random() >= 0.5);
    this.arregloUno = [ "foo", "bar", "baz" ];
    this.APIURL = $appSettings.URLBASE;
    this.APIUR2 = $appSettings.URLAUTH;
    this.IsProductive = $appSettings.ISPRODUCTIONVERSION;
    this.scotches = [ { name : "Macallan 12", price : 50 }, { name : "Chivas Regal Royal Salute", price : 10000 }, { name : "Glenfiddich 1937", price : 20000 } ];
  }
}
