export default class AboutController {
  // Constructor.
  constructor($appSettings) {
    this.nombre = "usuario";
    this.afirmacion = (Math.random() >= 0.5);
    this.arregloUno = [ "foo", "bar", "baz" ];
    this.APIURL = $appSettings.URLBASE;
    this.APIUR2 = $appSettings.URLAUTH;
    this.IsProductive = $appSettings.ISPRODUCTIONVERSION;
  }

  // Funcion del controlador.
  changeName() {
    this.nombre = "angular-tips";
    window.alert("Estatus: " + this.IsProductive + "\nDirección URL: " + this.APIURL + "\nDirección URL de autenticación: " + this.APIUR2);
  }

  // Mostrar nombre de manera aleatoria.
  randomName() {
    this.nombre = this.randomNames.getName();
  }

  randomBool() {
    this.afirmacion = (Math.random() >= 0.5);
  }
}
