/* Importando componentes de Node.js. */
import angular from "angular";
import ngRoute from "angular-ui-router";

/* Importando elementos del componente interno. */
import routing from "./about.routes";
import AboutController from "./about.controller";

/* Inyectando objetos al controlador. */
AboutController.$inject = [ "appSettings" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.about", [ ngRoute ]).config(routing).controller("AboutController", AboutController).name;
