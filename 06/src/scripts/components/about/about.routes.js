routes.$inject = [ "$stateProvider" ];

export default function routes($stateProvider) {
  $stateProvider.state("about", {
    url : "/about",
    views : {
      "" : {
        template : require("./about.handlebars"),
        controller : "AboutController",
        controllerAs : "vm"
      },
      "columnOne@about" : { template : "¡Mira! Soy una columna." },
      "columnTwo@about" : {
        template : require("./about.column2/about.column2.handlebars"),
        controller : "AboutColumn2Controller",
        controllerAs : "vm"
      }
    }
  });
}
