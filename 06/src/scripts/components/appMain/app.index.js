/* Importando componentes de Node.js. */
import angular from "angular";

/* Importando elementos del componente interno. */
import IndexController from "./app.controller";

/* Inyectando objetos al controlador. */
IndexController.$inject = [ "$state", "appAlerts", "appConstants" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.index", []).controller("IndexController", IndexController).name;
