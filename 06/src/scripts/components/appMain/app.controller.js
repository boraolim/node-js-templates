export default class IndexController {
  // Constructor.
  constructor($state, appAlerts, appConstants) {
    this.stateMenu = $state;
    this.isCollapsed = true;
    this.alerts = [];
    this.appName = appConstants.APPNAME;
    this.companyName = appConstants.COMPANY;
    this.version = appConstants.VERSION;
    this.author = appConstants.AUTHOR;
    this.currentYear = appConstants.CURRENTYEAR;
    this._appAlerts = appAlerts;
  }

  getClass(path) {
    if (this.stateMenu.current.name.substr(0, path.length) === path) {
      return "active";
    } else {
      return "";
    }
  }

  getAlerts() {
    this.alerts = this._appAlerts.get();
    return (this.alerts);
  }
}
