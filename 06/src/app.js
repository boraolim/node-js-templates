/* Importando componentes de AngularJS. */
import angular from "angular";
import ngResource from "angular-resource";
import ngRoute from "angular-ui-router";
import ngAnimate from "angular-animate";
import ngSanitize from "angular-sanitize";
import uibootstrap from "angularjs-ui-bootstrap";

/* Importando estilos CSS3 (general) */
import "./styles/styles.scss";

/* Importando elementos base de la aplicación de AngularJS. */
import configMain from "./app.config";
import runMain from "./app.run";

/* Servicios. */
import mainService from "./scripts/services/main.service";

/* Directivas. */
import aboutDrc from "./scripts/directives/about.directive";
import greetingDrc from "./scripts/directives/greeting.directive";

/* Filtros. */
import boolFtr from "./scripts/filters/boolean.filter";

/* Factories. */
import appSettings from "./scripts/factories/app.settings";
import appConstants from "./scripts/factories/app.constants";
import appAlerts from "./scripts/factories/app.alerts";
import httpIntercept from "./scripts/factories/app.interceptor";

/* Controladores | Componentes. */
import notFoundCtrl from "./scripts/components/404/404.index";
import mainCtrl from "./scripts/components/appMain/app.index";

import homeCtrl from "./scripts/components/home/home.index";
import homelistCtrl from "./scripts/components/home.list/home.list.index";
import homeparCtrl from "./scripts/components/home.paragraph/home.paragraph.index";
import contactCtrl from "./scripts/components/contact/contact.index";
import testapiCtrl from "./scripts/components/testapi/testapi.index";
import aboutCtrl from "./scripts/components/about/about.index";
import aboutCol2Ctrl from "./scripts/components/about/about.column2/about.column2.index";

/* Módulo principal de AngularJs */
angular.module("mainApp", [
  ngResource,
  ngRoute,
  ngAnimate,
  ngSanitize,
  uibootstrap,
  mainService,
  aboutDrc,
  greetingDrc,
  boolFtr,
  appSettings,
  appConstants,
  appAlerts,
  httpIntercept,
  notFoundCtrl,
  mainCtrl,
  homeCtrl,
  homelistCtrl,
  homeparCtrl,
  contactCtrl,
  testapiCtrl,
  aboutCtrl,
  aboutCol2Ctrl
]).config(configMain).run(runMain);

// Fin del script.
