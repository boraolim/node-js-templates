import "./styles/styles.scss";
import "./static/logo.png";
import "./static/babel.jpg";
import "./static/scss.svg";
import "./static/bootstrap.png";
import "./static/image.gif";
import "./static/image.jpg";
import "./static/image.png";
import "./static/image.webp";

import { showAlert } from "./scripts/messages";
import angular from "angular";
import uibs from "angularjs-ui-bootstrap";

// Declaramos una variable que lee el valor de una variable global.
// https://antoniomasia.com/que-es-eslint-y-por-que-deberias-usarlo/
var urlAPI1 = API_URL;
var arr = [ "foo", "bar", "baz" ];

// Contenido de JavaScript.
document.getElementById("btn-alert").addEventListener("click", showAlert);
document.getElementById("btn-alert-jquery").addEventListener("click", respondMouseOver);

function respondMouseOver() {
  window.alert("Servidor API: " + urlAPI1 + "\nArreglo: " + JSON.stringify(arr));
}

(function() {
  "use strict";

  // Módulo principal de AngularJs
  angular.module("app", [ uibs ]);
})();
