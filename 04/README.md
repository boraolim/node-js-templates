<h1>Plantilla Base No. 4 de una aplicación en WebPack</h1>
<p>Ejemplo base de un esqueleto de una plantilla Frontend con WebPack y sus dependencias. El objetivo es poder poner en marcha un proyecto con las siguientes características:</p>
<ul>
  <li>Javascript ES6 o Typescript.</li>
  <li>Less/Sass + PostCSS.</li>
  <li>Gestión de assets, imágenes y fuentes sobre todo.</li>
  <li>Rutas relativas para subdominios.</li>
  <li>Sourcemaps en desarrollo.</li>
  <li>Minificación, depuración y ofuscación en producción.</li>
</ul>
<h3>Índice</h3>
<p>El siguiente índice muestra todos los pasos que hay que seguir para comprender una configuración completa de Webpack.</p>
<ul>
  <li>Configuración básica, bundling con import/export y generación de el primer bundle. Output y generación del bundle.</li>
  <li>Generación de HTML con los bundles insertados y uso de HandleBars para crear plantillas HTML.</li>
  <li>ES6, Core JS, Babel y Typescript. ESLint y TSLint.</li>
  <li>Optimización de fuentes con Fontmin Loader.</li>
  <li>CSS: Style loader y CSS loader, PostCSS, SASS y Extract Plugin.</li>
  <li>Gestión de assets, optimización de imágenes con ImageLoader y carga de archivos con FileLoader. Output relativo de los assets.</li>
  <li>Diferentes Entornos: minificación y ofuscación para producción. Variables de entorno y webpack mege.</li>
  <li>Uso de componentes como Font Awesome 4.7.0 y Bootstrap 3.4.1 (solo estilos CSS).</li>
  <li>Uso de AngularJS, Angular UI y UI Bootstrap para AngularJS.</li>
</ul>
<p>Realizar la instralación de los siguientes componentes, en el orden siguiente:</p>
<ol>
  <li>Crear el archivo de configuración de Node.js: <br/><strong>npm init -y</strong></li>
  <li>Instalar webpack, webpack-cli y webpack-dev-server: <br/><strong>npm install --save-dev webpack webpack-cli webpack-dev-server</strong></li>
  <li>Instalar Fontmin Loader para webpack : <br/><strong>npm install --save-dev fontmin-webpack</strong></li>
  <li>Instalar HTML para webpack: <br/><strong>npm install --save-dev html-webpack-plugin</strong></li>
  <li>Instalar Handlebars para generación de plantillas HTML para webpack: <br/><strong>npm install --save-dev handlebars handlebars-loader</strong></li>
  <li>Instalar SASS y sus componentes para webpack: <br/><strong>npm install --save-dev style-loader css-loader sass node-sass postcss-loader sass-loader</strong></li>
  <li>Instalar componentes de carga de archivos y rutas para webpack: <br/><strong>npm install --save-dev file-loader url-loader autoprefixer</strong></li>
  <li>Instalar limpiador de archivos de pre-producción para webpack: <br/><strong>npm install --save-dev clean-webpack-plugin</strong></li>
  <li>Instalar extractor de CSS3 en archivos de producción: <br/><strong>npm install --save-dev mini-css-extract-plugin</strong></li>
  <li>Instalar utilería de webpack para comprimir archivos JavaScript: <br/><strong>npm install --save-dev uglifyjs-webpack-plugin</strong></li>
  <li>Instalar webpack merge para unir archivos de configuración: <br/><strong>npm install --save-dev webpack-merge</strong></li>
  <li>Instalar babel y componentes: <br/><strong>npm install --save-dev @babel/core @babel/polyfill @babel/preset-env babel-loader core-js</strong></li>
  <li>Instalar eslint y componentes: <br/><strong>npm install --save-dev eslint eslint-config-standard eslint-loader eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard</strong></li>
  <li>Instalar typscript y sus componentes: <br/><strong>npm install --save-dev typescript ts-loader</strong></li>
  <li>Instalar AngularJS y sus componentes: <br/><strong>npm install --save angular angular-ui angular-ui-router angularjs-ui-bootstrap</strong></li>
</ol>
<p>La estructura de esta carpeta ya está lista para compilarse y probarse. Esta es la estructura interna del repositorio:</p>
<p>Se usaron directamente los archivos SASS para <a href="https://fontawesome.com/how-to-use/on-the-web/using-with/sass">Font Awesome 4.7.0</a> y <a href="https://github.com/twbs/bootstrap-sass">Bootstrap SASS.</a></p>
<h3>Fuentes de consulta:</h3>
<ul>
  <li><a href="https://medium.com/@afdiaz_/webpack-paso-a-paso-2fdf095d5bdd">Webpack paso a paso.</a></li>
  <li><a href="https://antoniomasia.com/que-es-eslint-y-por-que-deberias-usarlo/">Qué es EsLint y por qué deberías usarlo.</a></li>
  <li><a href="https://www.gpolanco.com/proyecto-react-con-typescript/">Configurar Typescript en un proyecto de React</a></li>
  <li><a href="https://gist.github.com/tejasbubane/9ae4d2e099bef97b3b118f99a163294e">Creación de entornos para WebPack.</a></li>
  <li><a href="https://areknawo.com/how-to-setup-webpack-config/">How to setup Webpack config.</a></li>
  <li><a href="https://www.sitepoint.com/bundle-static-site-webpack/">How to Bundle a Simple Static Site Using Webpack.</a></li>
  <li><a href="https://github.com/johnagan/clean-webpack-plugin">Clean Webpack Plugin.</a></li>
  <li><a href="https://github.com/webpack/webpack-dev-server">Webpack Dev Server.</a></li>
  <li><a href="https://github.com/jantimon/html-webpack-plugin">HTML Webpack Plugin.</a></li>
  <li><a href="https://github.com/webpack-contrib/style-loader">Style Loader.</a></li>
  <li><a href="https://github.com/patrickhulce/fontmin-webpack">Fontmin Webpack.</a></li>
  <li><a href="https://webpack.js.org/plugins/uglifyjs-webpack-plugin/">UglifyJs Webpack.</a></li>
  <li><a href="https://github.com/zloirock/core-js">Core JS</a></li>
</ul>
<hr>
<p>&copy; Olimpo Bonilla Ramírez. México 2020.</p>
<hr>
