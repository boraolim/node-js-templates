<h1>Plantillas Node.js</h1>
<p>Plantillas de Node.js para guía de aprendizaje.</p>
<h3>Requisitos:</h3>
<ul>
  <li>Tener instalado <a href="https://nodejs.org/es/download/">Node.js</a>, sin importar el sistema operativo.</li>
  <li><a href="https://code.visualstudio.com/download/">Visual Studio Code</a> o cualquier editor de texto para la revisi&oacute;n de c&oacute;digo fuente.</li>
</ul>
<p>Favor de seguir cada una de las carpetas de este repositorio.</p>
<h3>Fuentes de consulta:</h3>
<ul>
  <li><a href="https://www.youtube.com/watch?v=vF2emKbaP4M">Curso b&aacute;sico de Webpack 4</a></li>
  <li><a href="https://areknawo.com/how-to-setup-webpack-config/">How to setup Webpack config.</a></li>
  <li><a href="https://www.sitepoint.com/bundle-static-site-webpack/">How to Bundle a Simple Static Site Using Webpack.</a></li>
  <li><a href="https://github.com/johnagan/clean-webpack-plugin">Clean-webpack-plugin.</a></li>
  <li><a href="https://github.com/webpack/webpack-dev-server">webpack-dev-server.</a></li>
  <li><a href="https://github.com/jantimon/html-webpack-plugin">HTML Webpack Plugin</a></li>
  <li><a href="https://github.com/webpack-contrib/style-loader">Style Loader</a></li>
  <li><a href="https://www.npmjs.com/package/browser-sync-webpack-plugin">Browser-sync para webpack</a></li>
</ul>
<p><hr>&copy; Olimpo Bonilla Ram&iacute;rez. 2020.</p>