/* Importando componentes de AngularJS. */
import angular from "angular";
import ngRoute from "angular-ui-router";
import ngAnimate from "angular-animate";
import ngSanitize from "angular-sanitize";
import uibs from "angularjs-ui-bootstrap";

/* Importando estilos CSS3 (general) */
import "./styles/styles.scss";

/* Importando elementos base de la aplicación de AngularJS. */
import appSettings from "./app.settings";
import routing from "./app.routing";

/* Servicios. */
import isCollapsed from "./scripts/services/isCollapsed.service";
import randomNames from "./scripts/services/randomNames.service";

/* Directivas. */
import greeting from "./scripts/directives/greeting.directive";

/* Filtros. */
import inputboolToText from "./scripts/filters/boolean.filter";

/* Controladores. */
import nvaBarCtrl from "./scripts/components/navbar/index";
import home from "./scripts/components/home/index";
import contact from "./scripts/components/contact/index";
import about from "./scripts/components/about/index";

/* Listado de componentes de la aplicación de AngularJS. */
var elementos = [ ngRoute, ngRoute, ngAnimate, ngSanitize, uibs, appSettings, inputboolToText, nvaBarCtrl, isCollapsed, randomNames, greeting, home, contact, about ];

/* Función principal. */
(function() {
  "use strict";

  // Módulo principal de AngularJs
  angular.module("mainApp", elementos).config(routing);
})();
