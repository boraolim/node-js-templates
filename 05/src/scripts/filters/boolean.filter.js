import angular from "angular";

// Definición de la funcionalidad del filtro de AngularJS.
function inputboolToText() {
  return function(input) {
    switch (input) {
    case false:
      return "NO";
    default:
      return "SI";
    }
  };
}

// Exportamos el filtro final.
export default angular.module("mainApp.filters.inputboolToText", []).filter("inputboolToText", inputboolToText).name;
