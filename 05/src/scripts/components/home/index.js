/* Importando componentes de Node.js. */
import angular from "angular";
import uirouter from "angular-ui-router";

/* Importando elementos del componente interno. */
import routing from "./home.routes";
import HomeController from "./home.controller";

/* Inyectando objetos al controlador. */
HomeController.$inject = [ "appSettings", "randomNames", "isCollapsed" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.home", [ uirouter ]).config(routing).controller("HomeController", HomeController).name;
