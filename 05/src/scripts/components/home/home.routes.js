routes.$inject = [ "$stateProvider" ];

export default function routes($stateProvider) {
  $stateProvider.state("home", {
    url : "/",
    template : require("./home.handlebars"),
    controller : "HomeController",
    controllerAs : "contact"
  });
}
