routes.$inject = [ "$stateProvider" ];

export default function routes($stateProvider) {
  $stateProvider.state("contact", {
    url : "/Contact",
    template : require("./contact.handlebars"),
    controller : "ContactController",
    controllerAs : "contact"
  });
}
