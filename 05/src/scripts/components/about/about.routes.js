routes.$inject = [ "$stateProvider" ];

export default function routes($stateProvider) {
  $stateProvider.state("about", {
    url : "/About",
    template : require("./about.handlebars"),
    controller : "AboutController",
    controllerAs : "about"
  });
}
