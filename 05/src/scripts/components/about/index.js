/* Importando componentes de Node.js. */
import angular from "angular";
import uirouter from "angular-ui-router";

/* Importando estilos CSS3 (general) */

/* Importando elementos base de la aplicación de AngularJS. */
import routing from "./about.routes";
import AboutController from "./about.controller";

/* Función principal para el controlador 'About'. */
export default angular.module("mainApp.about", [ uirouter ]).config(routing).controller("AboutController", AboutController).name;
