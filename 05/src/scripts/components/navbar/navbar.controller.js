export default class NavBarController {
  // Constructor.
  constructor($isCollapsed) {
    this.isCollapsed = !$isCollapsed.isCollapsed;
  }
}
