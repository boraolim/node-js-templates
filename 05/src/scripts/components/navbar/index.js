/* Importando componentes de Node.js. */
import angular from "angular";

/* Importando elementos del componente interno. */
import NavBarController from "./navbar.controller";

/* Inyectando objetos al controlador. */
NavBarController.$inject = [ "appSettings", "isCollapsed" ];

/* Función principal del controlador de AngularJS. */
export default angular.module("mainApp.navBarController", []).controller("NavBarController", NavBarController).name;
