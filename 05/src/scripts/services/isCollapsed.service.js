import angular from "angular";

class isCollapsed {
  // Constructor.
  constructor() {
    this.isCollapsed = true;
  }
}

// Exportamos el servicio.
export default angular.module("mainApp.services.isCollapsed", []).service("isCollapsed", isCollapsed).name;
