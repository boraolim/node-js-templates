import angular from "angular";

class RandomNames {
  // Constructor.
  constructor() {
    this.names = [ "Cruz Azul", "América", "Chivas", "Pumas", "Barcelona FC", "Real Madrid", "Toluca FC", "Puebla", "León", "Xolos de Tijuana" ];
  }

  // Función que muestra el nombre de manera aleatoria.
  getName() {
    const totalNames = this.names.length;
    const rand = Math.floor(Math.random() * totalNames);
    return this.names[rand];
  }
}

// Exportamos el servicio.
export default angular.module("mainApp.services.randomNames", []).service("randomNames", RandomNames).name;
