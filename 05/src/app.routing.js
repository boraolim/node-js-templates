routing.$inject = [ "$urlRouterProvider", "$locationProvider" ];

export default function routing($urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode({
    enabled : true,
    requireBase : true,
    rewriteLinks : true
  });
  $urlRouterProvider.otherwise("/");
}
