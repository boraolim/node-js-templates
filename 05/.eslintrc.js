/* eslint-disable no-multi-spaces */
/* Platilla esLint para la configuración y validación de JavaScript. */
/* Autor: Olimpo Bonilla Ramírez. */
// "off" or 0 - turn the rule off.
// "warn" or 1 - turn the rule on as a warning (doesn't affect exit code).
// "error" or 2 - turn the rule on as an error (exit code will be 1).

module.exports = {
  /* Entorno de desarrollo:
     https://eslint.org/docs/user-guide/configuring#specifying-environments */
  "env" : {
    "browser" : true,         /* El objetivo del código es ejecutar en navegadores */
    "es6" : true,             /* El código estará escrito en ES6 */
    "commonjs" : true,        /* Javascript común. */
    "jquery" : true           /* Si jQuery es requerido. */
  },
  /* Activar variables globales.
     https://stackoverflow.com/questions/39053562/eslint-no-undef-and-webpack-plugin */
  "globals" : {
    "API_URL" : true,
    "SERVICE_URL" : true,
    "IsProductive" : true
  },
  /* Activa las reglas marcadas con ✓ en la documentación oficial de ESLint:
     https://eslint.org/docs/rules/ y el paquete eslint-config-standard */
  "extends" : [ "eslint:recommended", "standard" ],
  /* Opciones de parseo:
     https://eslint.org/docs/user-guide/configuring#specifying-parser-options */
  "parserOptions" : {
    "ecmaVersion" : 2018,     /* Establece la versión de ECMAScript que se usará */
    "sourceType" : "module"   /* Indica si se usan módulos ES6 o solo scripts */
  },
  /* Reglas de ESLint personalizadas (sobreescriben a anteriores):
     https://eslint.org/docs/rules/ */
  "rules" : {
    /* Variables no definidas: https://eslint.org/docs/rules/no-undef */
    "no-undef" : [ "error", { "typeof" : true } ],
    /* Forzar la eclaración de variables en formato camelcase: https://eslint.org/docs/rules/camelcase */
    "camelcase" : [ "error", { "properties" : "always" } ],
    /* Indentación a 2 espacios: https://eslint.org/docs/rules/indent */
    "indent" : [ "error", 2 ],
    /* Finales de línea de UNIX: https://eslint.org/docs/rules/linebreak-style */
    "linebreak-style" : [ "error", "unix" ],
    /* Uso de comillas dobles para strings: https://eslint.org/docs/rules/quotes */
    "quotes" : [ "error", "double" ],
    /* Uso de punto y coma obligatorio: https://eslint.org/docs/rules/semi */
    "semi" : [ "error", "always" ],
    /* Evitar la redeclaración de variables: https://eslint.org/docs/rules/no-redeclare */
    "no-redeclare" : [ "error", { "builtinGlobals" : true } ],
    /* Permitir espacios en blanco al final de las líneas vacías: https://eslint.org/docs/rules/no-trailing-spaces */
    "no-trailing-spaces" : [ "error", { "skipBlankLines" : true } ],
    /* Imponer un espacio dentro de paréntesis: https://eslint.org/docs/rules/space-in-parens */
    "space-in-parens" : [ "error", "never" ],
    /* Rechazar nueva línea al final de los archivos: https://eslint.org/docs/rules/eol-last */
    "eol-last" : [ "error", "always" ],
    /* Requerir o rechazar un espacio antes de la función paréntesis: https://eslint.org/docs/rules/space-before-function-paren */
    "space-before-function-paren" : [ "error", "never" ],
    /* Requerir o rechazar el espacio entre identificadores de función y sus invocaciones (func-call-spacing): https://eslint.org/docs/rules/func-call-spacing */
    "func-call-spacing" : [ "error", "never" ],
    /* No permitir espacios mixtos y pestañas para sangría (sin espacios mixtos y pestañas): https://eslint.org/docs/rules/no-mixed-spaces-and-tabs */
    "no-mixed-spaces-and-tabs" : [ "error", "smart-tabs" ],
    /* No permitir espacios múltiples (sin espacios múltiples): https://eslint.org/docs/rules/no-multi-spaces */
    "no-multi-spaces" : [ "error", { "ignoreEOLComments" : false } ],
    /* Requerir comillas alrededor de nombres de propiedades literales de objetos (quote-props): https://eslint.org/docs/rules/quote-props */
    "quote-props" : [ "error", "consistent" ],
    /* No permitir o imponer espacios dentro de los corchetes (espaciado entre paréntesis): https://eslint.org/docs/rules/array-bracket-spacing */
    "array-bracket-spacing" : [ "error", "always" ],
    /* No permitir todas las pestañas: https://eslint.org/docs/rules/no-tabs */
    "no-tabs" : [ "error", { "allowIndentationTabs" : true } ],
    /* Requerir o rechazar el relleno dentro de bloques: https://eslint.org/docs/rules/padded-blocks */
    "padded-blocks" : [ "error", "never" ],
    /* Imponer un espacio constante entre claves y valores en las propiedades literales del objeto: https://eslint.org/docs/rules/key-spacing */
    "key-spacing" : [ "error", { "beforeColon" : true } ]
  }
};
