"use strict";

const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const FontminPlugin = require("fontmin-webpack");
const autoprefixer = require("autoprefixer");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const entryPath = ["@babel/polyfill", "./src/app.js"];
const basePath = __dirname;
const distPath = "../dist";

module.exports = {
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx"]
  },
  entry: entryPath,
  output:
  {
    path: path.join(basePath, distPath),
    filename: "js/bundle.js"
  },
  module:
  {
    rules:
    [
      {
        test: /(\.js|.jsx)$/i,
        exclude: /node_modules/,
        use:
        [
          { loader: "babel-loader" },
          "eslint-loader"
        ]
      },
      {
        test: /(\.ts|.tsx)$/i,
        exclude: /node_modules/,
        use: [ "ts-loader" ]
      },
      {
        test: /\.handlebars$/i,
        use:
        [
          { loader: "handlebars-loader" }
        ]
      },
      {
        test: /\.(sa|sc|c)ss$/i,
        exclude: /node_modules/,
        use:
        [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "postcss-loader",
            options:
            {
              autoprefixer: { browser: ["last 2 versions"] },
              plugins: () => [ autoprefixer ]
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.(jpg|jpeg|png|gif)$/i,
        use:
        [
          {
            loader: "file-loader",
            options:
            {
              name: "[name].[ext]",
              outputPath: "static/",
              useRelativePath: true
            }
          },
          {
            loader: "image-webpack-loader",
            options:
            {
              mozjpeg:
              {
                progressive: true,
                quality: 65
              },
              // optipng.enabled: false will disable optipng
              optipng:
              {
                enabled: false
              },
              pngquant:
              {
                quality: [0.65, 0.90],
                speed: 4
              },
              gifsicle:
              {
                interlaced: false,
              },
              // the webp option will enable WEBP
              webp:
              {
                quality: 75
              }
            }
          }
        ]
      },
      {
        test: /\.(eot|otf|svg|ttf|woff|woff2)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          useRelativePath: true
        }
      }
    ]
  },
  optimization: {
    noEmitOnErrors: true,
    minimizer:
    [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions:
        {
          compress: false,
          ecma: 6,
          mangle: true,
          output: {
            comments: false,
          },
        },
        sourceMap: true
      })
    ]
  },
  plugins:
  [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new FontminPlugin(),
    new HtmlWebpackPlugin({
      title: "Mi primera aplicación de WebPack",
      favicon: "./src/favicon.ico",
      template: "./src/index.handlebars",
      minify:
      {
        html5: true,
        collapseWhitespace: true,
        caseSensitive: true,
        removeComments: true,
        removeEmptyElements: true
      }
    }),
    new MiniCssExtractPlugin({
      filename: "css/[name]-styles.css",
      chunkFilename: "[id].css"
    }),
    new webpack.DefinePlugin({
      "API_URL": (process.env.NODE_ENV === "production") ? JSON.stringify("http://produccion.com") : JSON.stringify("http://desarrollo.com"),
      "SERVICE_URL": (process.env.NODE_ENV === "production") ? JSON.stringify("http://produccion.com") : JSON.stringify("http://desarrollo.com")
    })
  ]
};