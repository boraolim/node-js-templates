import "./styles/styles.scss";
import "./static/logo.png";

import { showAlert } from "./scripts/messages";
import $ from "jquery";
import { v4 } from "uuid";

// Declaramos una variable que lee el valor de una variable global.
// https://antoniomasia.com/que-es-eslint-y-por-que-deberias-usarlo/
var urlAPI1 = API_URL;
var arr = [ "foo", "bar", "baz" ];

// Contenido de JavaScript.
document.getElementById("btn-alert").addEventListener("click", showAlert);
$("#btn-alert-jquery").click(() => window.alert("Identificador unico (UUID): " + v4().toString().toUpperCase() +
                                                "\nServidor API: " + urlAPI1 + "\nArreglo: " + JSON.stringify(arr)));
window.alert("Prueba superada.");
